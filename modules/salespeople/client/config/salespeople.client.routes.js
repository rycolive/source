(function () {
  'use strict';

  angular
    .module('salespeople')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('salespeople', {
        abstract: true,
        url: '/salespeople',
        template: '<ui-view/>'
      })
      .state('salespeople.list', {
        url: '',
        templateUrl: 'modules/salespeople/client/views/list-salespeople.client.view.html',
        controller: 'SalespeopleListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Salespeople List'
        }
      })
      .state('salespeople.create', {
        url: '/create',
        templateUrl: 'modules/salespeople/client/views/form-salesperson.client.view.html',
        controller: 'SalespeopleController',
        controllerAs: 'vm',
        resolve: {
          salespersonResolve: newSalesperson
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Salespeople Create'
        }
      })
      .state('salespeople.edit', {
        url: '/:salespersonId/edit',
        templateUrl: 'modules/salespeople/client/views/form-salesperson.client.view.html',
        controller: 'SalespeopleController',
        controllerAs: 'vm',
        resolve: {
          salespersonResolve: getSalesperson
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Salesperson {{ salespersonResolve.name }}'
        }
      })
      .state('salespeople.view', {
        url: '/:salespersonId',
        templateUrl: 'modules/salespeople/client/views/view-salesperson.client.view.html',
        controller: 'SalespeopleController',
        controllerAs: 'vm',
        resolve: {
          salespersonResolve: getSalesperson
        },
        data: {
          pageTitle: 'Salesperson {{ articleResolve.name }}'
        }
      });
  }

  getSalesperson.$inject = ['$stateParams', 'SalespeopleService'];

  function getSalesperson($stateParams, SalespeopleService) {
    return SalespeopleService.get({
      salespersonId: $stateParams.salespersonId
    }).$promise;
  }

  newSalesperson.$inject = ['SalespeopleService'];

  function newSalesperson(SalespeopleService) {
    return new SalespeopleService();
  }
}());
