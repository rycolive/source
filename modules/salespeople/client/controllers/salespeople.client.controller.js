(function () {
  'use strict';

  // Salespeople controller
  angular
    .module('salespeople')
    .controller('SalespeopleController', SalespeopleController);

  SalespeopleController.$inject = ['$scope', '$state', 'Authentication', 'salespersonResolve'];

  function SalespeopleController ($scope, $state, Authentication, salesperson) {
    var vm = this;

    vm.authentication = Authentication;
    vm.salesperson = salesperson;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Salesperson
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.salesperson.$remove($state.go('salespeople.list'));
      }
    }

    // Save Salesperson
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.salespersonForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.salesperson._id) {
        vm.salesperson.$update(successCallback, errorCallback);
      } else {
        vm.salesperson.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('salespeople.view', {
          salespersonId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
