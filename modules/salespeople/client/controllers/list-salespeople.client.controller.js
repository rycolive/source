(function () {
  'use strict';

  angular
    .module('salespeople')
    .controller('SalespeopleListController', SalespeopleListController);

  SalespeopleListController.$inject = ['SalespeopleService'];

  function SalespeopleListController(SalespeopleService) {
    var vm = this;

    vm.salespeople = SalespeopleService.query();
  }
}());
