// Salespeople service used to communicate Salespeople REST endpoints
(function () {
  'use strict';

  angular
    .module('salespeople')
    .factory('SalespeopleService', SalespeopleService);

  SalespeopleService.$inject = ['$resource'];

  function SalespeopleService($resource) {
    return $resource('api/salespeople/:salespersonId', {
      salespersonId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
