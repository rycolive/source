(function () {
  'use strict';

  describe('Salespeople Route Tests', function () {
    // Initialize global variables
    var $scope,
      SalespeopleService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _SalespeopleService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      SalespeopleService = _SalespeopleService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('salespeople');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/salespeople');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          SalespeopleController,
          mockSalesperson;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('salespeople.view');
          $templateCache.put('modules/salespeople/client/views/view-salesperson.client.view.html', '');

          // create mock Salesperson
          mockSalesperson = new SalespeopleService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Salesperson Name'
          });

          // Initialize Controller
          SalespeopleController = $controller('SalespeopleController as vm', {
            $scope: $scope,
            salespersonResolve: mockSalesperson
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:salespersonId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.salespersonResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            salespersonId: 1
          })).toEqual('/salespeople/1');
        }));

        it('should attach an Salesperson to the controller scope', function () {
          expect($scope.vm.salesperson._id).toBe(mockSalesperson._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/salespeople/client/views/view-salesperson.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          SalespeopleController,
          mockSalesperson;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('salespeople.create');
          $templateCache.put('modules/salespeople/client/views/form-salesperson.client.view.html', '');

          // create mock Salesperson
          mockSalesperson = new SalespeopleService();

          // Initialize Controller
          SalespeopleController = $controller('SalespeopleController as vm', {
            $scope: $scope,
            salespersonResolve: mockSalesperson
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.salespersonResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/salespeople/create');
        }));

        it('should attach an Salesperson to the controller scope', function () {
          expect($scope.vm.salesperson._id).toBe(mockSalesperson._id);
          expect($scope.vm.salesperson._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/salespeople/client/views/form-salesperson.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          SalespeopleController,
          mockSalesperson;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('salespeople.edit');
          $templateCache.put('modules/salespeople/client/views/form-salesperson.client.view.html', '');

          // create mock Salesperson
          mockSalesperson = new SalespeopleService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Salesperson Name'
          });

          // Initialize Controller
          SalespeopleController = $controller('SalespeopleController as vm', {
            $scope: $scope,
            salespersonResolve: mockSalesperson
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:salespersonId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.salespersonResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            salespersonId: 1
          })).toEqual('/salespeople/1/edit');
        }));

        it('should attach an Salesperson to the controller scope', function () {
          expect($scope.vm.salesperson._id).toBe(mockSalesperson._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/salespeople/client/views/form-salesperson.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
