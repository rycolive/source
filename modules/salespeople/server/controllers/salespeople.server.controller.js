'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Salesperson = mongoose.model('Salesperson'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');
var sql = require('mssql');
var config = require(path.resolve('./config/config'));
var async = require('async');
var fs = require('fs');

/**
 * List of Salespeople
 */
exports.list = function(req, res) {
  // get query SQL file
  var querysql = fs.readFileSync('./modules/salespeople/server/sql/GetAll_SalesPerson.sql').toString();

  // create connection to RHAU_M3 db
  var connection = new sql.Connection(config.mssqldb_m3, function(err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);

    // exec query
    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};
