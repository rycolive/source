'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Salesperson Schema
 */
var SalespersonSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Salesperson name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Salesperson', SalespersonSchema);
