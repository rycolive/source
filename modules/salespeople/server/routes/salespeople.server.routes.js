'use strict';

/**
 * Module dependencies
 */
var salespeoplePolicy = require('../policies/salespeople.server.policy'),
  salespeople = require('../controllers/salespeople.server.controller');

module.exports = function(app) {
  // Salespeople Routes
  app.route('/api/salespeople').all(salespeoplePolicy.isAllowed)
    .get(salespeople.list);
};
