(function () {
  'use strict';

  describe('Warehouses Route Tests', function () {
    // Initialize global variables
    var $scope,
      WarehousesService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _WarehousesService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      WarehousesService = _WarehousesService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('warehouses');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/warehouses');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          WarehousesController,
          mockWarehouse;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('warehouses.view');
          $templateCache.put('modules/warehouses/client/views/view-warehouse.client.view.html', '');

          // create mock Warehouse
          mockWarehouse = new WarehousesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Warehouse Name'
          });

          // Initialize Controller
          WarehousesController = $controller('WarehousesController as vm', {
            $scope: $scope,
            warehouseResolve: mockWarehouse
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:warehouseId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.warehouseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            warehouseId: 1
          })).toEqual('/warehouses/1');
        }));

        it('should attach an Warehouse to the controller scope', function () {
          expect($scope.vm.warehouse._id).toBe(mockWarehouse._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/warehouses/client/views/view-warehouse.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          WarehousesController,
          mockWarehouse;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('warehouses.create');
          $templateCache.put('modules/warehouses/client/views/form-warehouse.client.view.html', '');

          // create mock Warehouse
          mockWarehouse = new WarehousesService();

          // Initialize Controller
          WarehousesController = $controller('WarehousesController as vm', {
            $scope: $scope,
            warehouseResolve: mockWarehouse
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.warehouseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/warehouses/create');
        }));

        it('should attach an Warehouse to the controller scope', function () {
          expect($scope.vm.warehouse._id).toBe(mockWarehouse._id);
          expect($scope.vm.warehouse._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/warehouses/client/views/form-warehouse.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          WarehousesController,
          mockWarehouse;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('warehouses.edit');
          $templateCache.put('modules/warehouses/client/views/form-warehouse.client.view.html', '');

          // create mock Warehouse
          mockWarehouse = new WarehousesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Warehouse Name'
          });

          // Initialize Controller
          WarehousesController = $controller('WarehousesController as vm', {
            $scope: $scope,
            warehouseResolve: mockWarehouse
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:warehouseId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.warehouseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            warehouseId: 1
          })).toEqual('/warehouses/1/edit');
        }));

        it('should attach an Warehouse to the controller scope', function () {
          expect($scope.vm.warehouse._id).toBe(mockWarehouse._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/warehouses/client/views/form-warehouse.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
