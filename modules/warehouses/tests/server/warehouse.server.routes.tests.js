'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  warehouse;

/**
 * Warehouse routes tests
 */
describe('Warehouse CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  it('should be able to get a list of Warehouses if not signed in', function (done) {
    // Request Warehouses
    agent.get('/api/warehouses')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Array);

        // Call the assertion callback
        done();
      });
  });
});
