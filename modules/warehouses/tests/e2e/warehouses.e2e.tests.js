'use strict';

describe('Warehouses E2E Tests:', function () {
  describe('Test Warehouses page', function () {
    it('Should report missing credentials', function () {
      browser.get('http://localhost:3001/warehouses');
      expect(element.all(by.repeater('warehouse in warehouses')).count()).toEqual(0);
    });
  });
});
