(function () {
  'use strict';

  // Warehouses controller
  angular
    .module('warehouses')
    .controller('WarehousesController', WarehousesController);

  WarehousesController.$inject = ['$scope', '$state', 'Authentication', 'warehouseResolve'];

  function WarehousesController ($scope, $state, Authentication, warehouse) {
    var vm = this;

    vm.authentication = Authentication;
    vm.warehouse = warehouse;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Warehouse
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.warehouse.$remove($state.go('warehouses.list'));
      }
    }

    // Save Warehouse
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.warehouseForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.warehouse._id) {
        vm.warehouse.$update(successCallback, errorCallback);
      } else {
        vm.warehouse.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('warehouses.view', {
          warehouseId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
