(function () {
  'use strict';

  angular
    .module('warehouses')
    .controller('WarehousesListController', WarehousesListController);

  WarehousesListController.$inject = ['WarehousesService'];

  function WarehousesListController(WarehousesService) {
    var vm = this;

    vm.warehouses = WarehousesService.query();
  }
}());
