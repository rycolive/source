(function () {
  'use strict';

  angular
    .module('warehouses')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('warehouses', {
        abstract: true,
        url: '/warehouses',
        template: '<ui-view/>'
      })
      .state('warehouses.list', {
        url: '',
        templateUrl: 'modules/warehouses/client/views/list-warehouses.client.view.html',
        controller: 'WarehousesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Warehouses List'
        }
      })
      .state('warehouses.create', {
        url: '/create',
        templateUrl: 'modules/warehouses/client/views/form-warehouse.client.view.html',
        controller: 'WarehousesController',
        controllerAs: 'vm',
        resolve: {
          warehouseResolve: newWarehouse
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Warehouses Create'
        }
      })
      .state('warehouses.edit', {
        url: '/:warehouseId/edit',
        templateUrl: 'modules/warehouses/client/views/form-warehouse.client.view.html',
        controller: 'WarehousesController',
        controllerAs: 'vm',
        resolve: {
          warehouseResolve: getWarehouse
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Warehouse {{ warehouseResolve.name }}'
        }
      })
      .state('warehouses.view', {
        url: '/:warehouseId',
        templateUrl: 'modules/warehouses/client/views/view-warehouse.client.view.html',
        controller: 'WarehousesController',
        controllerAs: 'vm',
        resolve: {
          warehouseResolve: getWarehouse
        },
        data: {
          pageTitle: 'Warehouse {{ articleResolve.name }}'
        }
      });
  }

  getWarehouse.$inject = ['$stateParams', 'WarehousesService'];

  function getWarehouse($stateParams, WarehousesService) {
    return WarehousesService.get({
      warehouseId: $stateParams.warehouseId
    }).$promise;
  }

  newWarehouse.$inject = ['WarehousesService'];

  function newWarehouse(WarehousesService) {
    return new WarehousesService();
  }
}());
