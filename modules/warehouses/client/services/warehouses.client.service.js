// Warehouses service used to communicate Warehouses REST endpoints
(function () {
  'use strict';

  angular
    .module('warehouses')
    .factory('WarehousesService', WarehousesService);

  WarehousesService.$inject = ['$resource'];

  function WarehousesService($resource) {
    return $resource('api/warehouses/:warehouseId', {
      warehouseId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
