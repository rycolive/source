SELECT 
	MWWHLO
	, MWWHLO + ' - ' + MWWHNM AS NAME 
FROM 
	MITWHL 
WHERE 
	(MWFACI = '110' OR (MWFACI = '100' AND MWWHLO < '090'))
AND 
	MWWHNM NOT LIKE '%CLOSED%'
ORDER BY
	NAME