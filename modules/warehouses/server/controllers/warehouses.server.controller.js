'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Warehouse = mongoose.model('Warehouse'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');
var sql = require('mssql');
var config = require(path.resolve('./config/config'));
var async = require('async');
var fs = require('fs');

/**
 * List of Warehouses
 */
exports.list = function(req, res) {
  // get query SQL file
  var querysql = fs.readFileSync('./modules/warehouses/server/sql/GetAll_Warehouse.sql').toString();

  // connect to RHAU_M3 db
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);

    // exec the query
    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};
