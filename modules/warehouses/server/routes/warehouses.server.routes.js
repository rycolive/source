'use strict';

/**
 * Module dependencies
 */
var warehousesPolicy = require('../policies/warehouses.server.policy'),
  warehouses = require('../controllers/warehouses.server.controller');

module.exports = function(app) {
  // Warehouses Routes
  app.route('/api/warehouses').all(warehousesPolicy.isAllowed)
    .get(warehouses.list);
};
