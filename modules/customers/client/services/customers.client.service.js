// Customers service used to communicate Customers REST endpoints
(function () {
  'use strict';

  angular
    .module('customers')
    .factory('CustomersService', CustomersService)
    .factory('GetCustomerByIdService', GetCustomerByIdService)
    .factory('GetCustomerOrderHistoryService', GetCustomerOrderHistoryService)
    .factory('GetOrderHistoryByCustOrderNoService', GetOrderHistoryByCustOrderNoService)
    .factory('CustomerSearchHistory', CustomerSearchHistory);

  CustomersService.$inject = ['$resource'];
  GetCustomerByIdService.$inject = ['$resource'];
  GetCustomerOrderHistoryService.$inject = ['$resource'];
  GetOrderHistoryByCustOrderNoService.$inject = ['$resource'];
  CustomerSearchHistory.$inject = ['$resource'];

  function CustomersService($resource) {
    return $resource('api/customers/:customerId', {
      customerId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  function GetCustomerByIdService($resource) {
    return $resource('api/customerbyid/:customerId', {
      customerId: '@customerId'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function GetCustomerOrderHistoryService($resource) {
    return $resource('api/customerorderhistory/:customer', {
      customer: '@customer'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function GetOrderHistoryByCustOrderNoService($resource) {
    return $resource('api/customerordernodetails/:customerOrderNo', {
      customerOrderNo: '@customerOrderNo'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function CustomerSearchHistory($resource) {
    return $resource('api/customersearchhistory/:userIdcustomer', {
      userIdcustomer: '@userId'
    }, {
      get: {
        method: 'GET'
      }
    });
  }
}());
