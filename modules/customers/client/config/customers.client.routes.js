(function () {
  'use strict';

  angular
    .module('customers')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('customers', {
        abstract: true,
        url: '/customers',
        template: '<ui-view/>'
      })
      .state('customers.list', {
        url: '',
        templateUrl: 'modules/customers/client/views/list-customers.client.view.html',
        controller: 'CustomersListController',
        controllerAs: 'vm',
        resolve: {
          customerResolve: newCustomer
        },
        data: {
          pageTitle: 'Customers List'
        },
        breadcrumb: {
          label: 'Customer List',
          parent: 'home'
        }
      })
      .state('customers.customerdetails', {
        url: '/:customerId',
        templateUrl: 'modules/customers/client/views/list-customers.client.view.html',
        controller: 'CustomersListController',
        controllerAs: 'vm',
        resolve: {
          customerResolve: getCustomer
        },
        data: {
          pageTitle: 'Customers List'
        },
        breadcrumb: {
          label: 'Customer List',
          parent: 'home'
        }
      })
      .state('customers.create', {
        url: '/create',
        templateUrl: 'modules/customers/client/views/form-customer.client.view.html',
        controller: 'CustomersController',
        controllerAs: 'vm',
        resolve: {
          customerResolve: newCustomer
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Customers Create'
        }
      })
      .state('customers.edit', {
        url: '/:customerId/edit',
        templateUrl: 'modules/customers/client/views/form-customer.client.view.html',
        controller: 'CustomersController',
        controllerAs: 'vm',
        resolve: {
          customerResolve: getCustomer
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Customer {{ customerResolve.name }}'
        }
      })
      .state('customers.view', {
        url: '/:customerId',
        templateUrl: 'modules/customers/client/views/view-customer.client.view.html',
        controller: 'CustomersController',
        controllerAs: 'vm',
        resolve: {
          customerResolve: getCustomer
        },
        data: {
          pageTitle: 'Customer {{ customerResolve.OKCUNM }}'
        },
        breadcrumb: {
          label: 'Customer Details',
          parent: 'customers.list'
        }
      })
      .state('customers.saleshistory', {
        url: '/saleshistory/:customerOrderNo',
        templateUrl: 'modules/customers/client/views/view-customer-order-history-details.client.view.html',
        controller: 'CustomersController',
        controllerAs: 'vm',
        resolve: {
          orderhistoryResolve: getOrderHistory
        },
        data: {
          pageTitle: 'Customer {{ orderhistoryResolve.OACUNO }} - {{ orderhistoryResolve.CustomerOrder }}'
        },
        breadcrumb: {
          label: 'Customer Sales History',
          parent: 'customers.list'
        }
      })
      .state('customers.searchhistory', {
        url: '/search/history',
        templateUrl: 'modules/customers/client/views/list-customers-search-history.client.view.html',
        controller: 'CustomersSearchHistoryController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Customer Search History'
        },
        breadcrumb: {
          label: 'Customer Search History',
          parent: 'customers.list'
        }
      });
  }

  getCustomer.$inject = ['$stateParams', 'GetCustomerByIdService'];

  function getCustomer($stateParams, GetCustomerByIdService) {
    return GetCustomerByIdService.query({
      customerId: $stateParams.customerId
    }).$promise;
  }

  newCustomer.$inject = ['CustomersService'];

  function newCustomer(CustomersService) {
    return new CustomersService();
  }

  getOrderHistory.$inject = ['$stateParams', 'GetOrderHistoryByCustOrderNoService'];

  function getOrderHistory($stateParams, GetOrderHistoryByCustOrderNoService) {
    return GetOrderHistoryByCustOrderNoService.query({
      customerOrderNo: $stateParams.customerOrderNo
    }).$promise;
  }
}());
