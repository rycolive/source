(function () {
  'use strict';

  angular
    .module('customers')
    .controller('CustomersListController', CustomersListController);

  CustomersListController.$inject = ['CustomersService', '$scope', '$filter', '$state', '$stateParams', '$window', '$http', '$resource', 'customerResolve', 'GetCustomerByIdService', 'GetCustomerOrderHistoryService', 'Authentication'];

  function CustomersListController(CustomersService, $scope, $filter, $state, $stateParams, $window, $http, $resource, customer, GetCustomerByIdService, GetCustomerOrderHistoryService, Authentication) {
    var vm = this;
    vm.authentication = Authentication;
    vm.dataLoad = false;
    vm.dataLoadSearch = true;
    vm.displayCustomerDetails = false;

    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      var element = $window.document.getElementById('customerSearch');
      if (element && !element.activeElement) {
        element.focus();
      }
    }

    $scope.getCustomerSearchList = function (searchString) {
      // call the search
      return $http.get('/api/customersearch', {
        params: {
          searchString: searchString
        }
      }).then(function(response) {
        vm.dataLoadSearch = true;
        return response.data;
      });
    };

    /* CustomersService.query(function(data) {
      vm.customers = data;
      console.log(data);
      if ($scope.item == null) {
        $scope.item = '';
      }

      if (customer.$promise !== undefined) {
        $scope.item = customer[0];
        $scope.getCustomerDetails($scope.item);
        vm.dataLoadSearch = true;
      }

      vm.dataLoadSearch = true;
    });*/

    $scope.getCustomerDetails = function (customer) {
      vm.displayCustomerDetails = true;

      // get customer details
      GetCustomerByIdService.query({ customerId: customer.OKCUNO }, function(data) {
        vm.customer = data[0];

        // get customer order history
        GetCustomerOrderHistoryService.query({
          customer: data[0].OKCUNO
        }, function(data) {
          vm.orderHistory = data;
          vm.buildPager();

          // insert into customer search history
          $http.post('/api/customersearchhistory', {
            customer: vm.customer,
            user: vm.authentication.user
          }).success(function(response) {
            $scope.item = '';
            console.log('success: ' + response);
          }).error(function(err) {
            console.log(err);
          });
        });
      });
    };

    if (customer.$promise !== undefined) {
      if ($scope.item == null) {
        $scope.item = '';
      }
      $scope.getCustomerDetails(customer[0]);
      vm.dataLoadSearch = true;
    }

    vm.buildPager = function () {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;
      vm.maxSize = 10;
      vm.figureOutItemsToDisplay();
    };

    vm.figureOutItemsToDisplay = function () {
      vm.filteredItems = $filter('filter')(vm.orderHistory);
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
      vm.dataLoad = true;
    };

    vm.pageChanged = function () {
      vm.figureOutItemsToDisplay();
    };
  }
}());
