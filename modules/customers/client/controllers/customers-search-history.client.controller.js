(function () {
  'use strict';

  // Customers controller
  angular
    .module('customers')
    .controller('CustomersSearchHistoryController', CustomersSearchHistoryController);

  CustomersSearchHistoryController.$inject = ['$scope', '$state', '$filter', 'Authentication', 'CustomerSearchHistory'];

  function CustomersSearchHistoryController ($scope, $state, $filter, Authentication, CustomerSearchHistory) {
    var vm = this;
    vm.authentication = Authentication;
    vm.dataLoad = false;

    CustomerSearchHistory.query({ userIdcustomer: vm.authentication.user.userCode }, function(data) {
      vm.searchHistory = data;
      vm.buildPager();
    });

    vm.buildPager = function () {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;
      vm.maxSize = 10;
      vm.figureOutItemsToDisplay();
    };

    vm.figureOutItemsToDisplay = function () {
      vm.filteredItems = $filter('filter')(vm.searchHistory, {
        $: vm.search
      });
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
      vm.dataLoad = true;
    };

    vm.pageChanged = function () {
      vm.figureOutItemsToDisplay();
    };
  }
}());
