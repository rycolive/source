(function () {
  'use strict';

  // Customers controller
  angular
    .module('customers')
    .controller('CustomersController', CustomersController);

  CustomersController.$inject = ['$scope', '$state', 'Authentication', 'orderhistoryResolve'];

  function CustomersController ($scope, $state, Authentication, order) {
    var vm = this;

    vm.authentication = Authentication;

    if (order !== undefined) {
      vm.customer = order[0];
    }
  }
}());
