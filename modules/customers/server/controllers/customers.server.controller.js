'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Customer = mongoose.model('Customer'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');
var sql = require('mssql');
var config = require(path.resolve('./config/config'));
var async = require('async');
var fs = require('fs');

/**
 * Create a Customer
 */
/* exports.create = function(req, res) {
  var customer = new Customer(req.body);
  customer.user = req.user;

  customer.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customer);
    }
  });
};*/

/**
 * Show the current Customer
 */
/* exports.read = function(req, res) {
  // convert mongoose document to JSON
  var customer = req.customer ? req.customer.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  customer.isCurrentUserOwner = req.user && customer.user && customer.user._id.toString() === req.user._id.toString();

  res.jsonp(customer);
};*/

/**
 * Update a Customer
 */
/* exports.update = function(req, res) {
  var customer = req.customer;

  customer = _.extend(customer, req.body);

  customer.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customer);
    }
  });
};*/

/**
 * Delete an Customer
 */
/* exports.delete = function(req, res) {
  var customer = req.customer;

  customer.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customer);
    }
  });
};*/

/**
 * List of Customers
 */
exports.list = function(req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/GetAll_Customer.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

/**
 * Customer middleware
 */
exports.customerByID = function(req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/Get_CustomerById.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('customerId', sql.VarChar(50), req.params.customerId);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else if (recordset.length !== 0) {
          res.json(recordset);
        } else {
          res.status(400).send({
            message: 'Customer with ID ' + req.params.customerId + ' is not found'
          });
        }
      });
  });
};

exports.customerAutocomplete = function(req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/Get_CustomerAutocomplete.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('searchString', sql.VarChar(50), req.query.searchString);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.jsonp(recordset);
        }
      });
  });
};

exports.customerOrderHistory = function(req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/Get_CustomerOrderHistoryByCustomerId.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('customerId', sql.VarChar(50), req.params.customer);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

exports.customerOrderHistoryByCustOrderNo = function (req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/Get_CustomerOrderHistoryByCustOrderNo.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('customerOrderNo', sql.VarChar(50), req.params.customerOrderNo);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

exports.addCustomerSearch = function (req, res) {
  var connection = new sql.Connection(config.mssqldb_application, function (err) {
    if (err) {
      return res.send(err.message);
    }

    // check if search history is exists, if yes, then update the registered date
    var checkHistoryQuerySQL = fs.readFileSync('./modules/customers/server/sql/Get_CustomerSearch_History_ByCustId_UserId.sql').toString();
    var getHistoryRequest = new sql.Request(connection);

    getHistoryRequest.input('customerId', req.body.customer.OKCUNO);
    getHistoryRequest.input('userCode', req.body.user.userCode);

    // get customer search history record by customer and registerdby
    getHistoryRequest.query(checkHistoryQuerySQL, function(err, historyRecord) {
      if (err) {
        console.log(err);
        return res.send(err.message);
      } else {
        // check if historyRecord is exists
        if (historyRecord.length !== 0) {
          // history record is exists
          // update registered history record
          var updateHistorySQL = fs.readFileSync('./modules/customers/server/sql/Update_CustomerSearch_History.sql').toString();

          // sql transaction
          var transactionUpdate = connection.transaction();
          transactionUpdate.begin(function(err) {
            if (err) {
              console.log('Begin Error: ' + err);
            }

            var inserted = 0;
            var rolledBack = false;

            // define transaction rollback function
            transactionUpdate.on('rollback', function(aborted) {
              console.log('rollback at update method');
              console.log(aborted);
              rolledBack = true;
            });

            var updateRequest = new sql.Request(transactionUpdate);

            // sql params customer, registered, registeredby
            updateRequest.input('customerId', sql.VarChar(50), req.body.customer.OKCUNO);
            updateRequest.input('registered', sql.DateTime2, new Date());
            updateRequest.input('registeredBy', sql.VarChar(50), req.body.user.userCode);

            // update Registered on IVOS_app_customersearch_history filtered by customer and registeredby
            updateRequest.query(updateHistorySQL,
              function(err, updateRecordset, affectedRow) {
                if (err) {
                  if (!rolledBack) {
                    transactionUpdate.rollback(function(err) {
                      console.log('==================== rolled back update transaction ======================');
                      console.log(err);
                    });
                  }

                  // update sql database failed
                  console.log(err);
                  return res.send(err);
                } else {
                  transactionUpdate.commit(function(err, updateRecordset) {
                    if (err) {
                      if (!rolledBack) {
                        transactionUpdate.rollback(function(err) {
                          console.log('==================== rolled back update transaction ======================');
                          console.log(err);
                        });
                      }
                    }

                    console.log('SQL Transaction committed. IVOS_app_customersearch_history updated');
                  });

                  res.send({ affectedRow: affectedRow });
                }
              });
          });
        } else {
          // history record not found
          // insert new history record
          var querysql = fs.readFileSync('./modules/customers/server/sql/Insert_CustomerSearch_History.sql').toString();

          // sql transaction
          var transaction = connection.transaction();
          transaction.begin(function(err) {
            if (err) {
              console.log('Begin Error: ' + err);
            }

            var inserted = 0;
            var rolledBack = false;

            // define transaction rollback function
            transaction.on('rollback', function(aborted) {
              console.log('rollback at insert method');
              console.log(aborted);
              rolledBack = true;
            });

            var request = new sql.Request(transaction);

            // sql params
            request.input('customerId', sql.VarChar(50), req.body.customer.OKCUNO);
            request.input('customerName', sql.VarChar(50), req.body.customer.OKCUNM);
            request.input('customerStatus', sql.VarChar(50), req.body.customer.OKSTAT);
            request.input('customerPriceType', sql.VarChar(50), req.body.customer.OKCFC1);
            request.input('customerPhone', sql.VarChar(50), req.body.customer.OKPHNO);
            request.input('customerStop', sql.VarChar(50), req.body.customer.OKBLCD);
            request.input('registerDate', sql.DateTime2, new Date());
            request.input('registerBy', sql.VarChar(50), req.body.user.userCode);

            // insert into IVOS_app_customersearch_history
            request.query(querysql,
            function(err, recordset, affectedRow) {
              console.log(recordset);
              if (err) {
                if (!rolledBack) {
                  transaction.rollback(function(err) {
                    console.log('==================== rolled back insert transaction ======================');
                    console.log(err);
                  });
                }

                // insert into sql database failed
                console.log(err);
                return res.send(err);
              } else {
                transaction.commit(function(err, recordset) {
                  if (err) {
                    if (!rolledBack) {
                      transaction.rollback(function(err) {
                        console.log('==================== rolled back insert transaction ======================');
                        console.log(err);
                      });
                    }
                  }

                  console.log('SQL Transaction committed.');
                });

                res.send({ affectedRow: affectedRow });
              }
            });
          });
        }
      }
    });
  });
};

exports.getCustomerSearchHistoryByUserId = function (req, res) {
  var querysql = fs.readFileSync('./modules/customers/server/sql/Get_CustomerSearch_History_ByUserId.sql').toString();
  var connection = new sql.Connection(config.mssqldb_application, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('userId', sql.VarChar(50), req.params.userIdcustomer);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};
