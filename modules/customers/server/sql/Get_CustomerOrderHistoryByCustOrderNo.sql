SELECT TOP 50 
	RTRIM(OACUOR) AS 'CustomerOrder', 
	OACUNO AS 'CustomerId',
	OAORNO AS 'Order', 
	OAWHLO + ' - ' + RTRIM(MWWHNM) AS 'Warehouse', 
	OAORTP AS 'OrderType', 
	OAORST AS 'HighestStatus', 
	OAORSL AS 'LowestStatus', 
	OAORDT,
	convert(date, convert(varchar, OAORDT)) AS 'OrderDate', 
	OANTLA AS 'TotalNet'
FROM 
	 OOHEAD, 
	 MITWHL 
WHERE 
	 OAWHLO = MWWHLO 
	 AND OACUOR = @customerOrderNo
ORDER BY 
	OrderDate DESC