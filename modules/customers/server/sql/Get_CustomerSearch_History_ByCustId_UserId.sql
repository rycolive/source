SELECT
	Customer,
	Name,
	Status,
	PriceType,
	PhoneNumber,
	CustomerStop,
	Registered,
	RegisteredBy
FROM
	IVOS_app_customersearch_history
WHERE
	RegisteredBy = @userCode
AND
	Customer = @customerId