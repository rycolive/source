INSERT INTO
	IVOS_app_customersearch_history
VALUES
	(
		@customerId,
		@customerName,
		@customerStatus,
		@customerPriceType,
		@customerPhone,
		@customerStop,
		@registerDate,
		@registerBy
	)