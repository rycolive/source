UPDATE
	IVOS_app_customersearch_history
SET
	Registered = @registered
WHERE
	Customer = @customerId
AND
	RegisteredBy = @registeredBy