SELECT TOP 50 
	RTRIM(OACUOR) AS 'CustomerOrder', 
	OAORNO AS 'Order', 
	OAWHLO + ' - ' + RTRIM(MWWHNM) AS 'Warehouse', 
	OAORTP AS 'OrderType', 
	OAORST AS 'HighestStatus', 
	OAORSL AS 'LowestStatus', 
	OAORDT,
	convert(date, convert(varchar, OAORDT)) AS 'OrderDate', 
	OANTLA AS 'TotalNet'
FROM 
	 OOHEAD, 
	 MITWHL 
WHERE 
	 OAWHLO = MWWHLO 
	 AND OACUNO = @customerId
ORDER BY 
	OrderDate DESC