'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Customers Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/customers',
      permissions: '*'
    }, {
      resources: '/api/customers/:customerId',
      permissions: '*'
    }, {
      resources: '/api/customersearchhistory',
      permissions: '*'
    }, {
      resources: '/api/customerbyid/:customerId',
      permissions: '*'
    }, {
      resources: '/api/customerorderhistory/:customer',
      permissions: '*'
    }, {
      resources: '/api/customerordernodetails/:customerOrderNo',
      permissions: '*'
    }, {
      resources: '/api/customersearchhistory/:userIdcustomer',
      permissions: '*'
    }, {
      resources: '/api/customersearch',
      permissions: ['get']
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/customers',
      permissions: ['get']
    }, {
      resources: '/api/customers/:customerId',
      permissions: ['get']
    }, {
      resources: '/api/customersearchhistory',
      permissions: ['post']
    }, {
      resources: '/api/customerbyid/:customerId',
      permissions: ['get']
    }, {
      resources: '/api/customerorderhistory/:customer',
      permissions: ['get']
    }, {
      resources: '/api/customerordernodetails/:customerOrderNo',
      permissions: ['get']
    }, {
      resources: '/api/customersearchhistory/:userIdcustomer',
      permissions: ['get']
    }, {
      resources: '/api/customersearch',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Customers Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? ['user'] : ['guest'];

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
