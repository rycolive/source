'use strict';

/**
 * Module dependencies
 */
var customersPolicy = require('../policies/customers.server.policy'),
  customers = require('../controllers/customers.server.controller');

module.exports = function(app) {
  // Customers Routes
  app.route('/api/customers').all(customersPolicy.isAllowed)
    .get(customers.list);
    // .post(customers.create);

  /* app.route('/api/customers/:customerId').all(customersPolicy.isAllowed)
    .get(customers.read)
    .put(customers.update)
    .delete(customers.delete);*/

  app.route('/api/customerbyid/:customerId').all(customersPolicy.isAllowed)
    .get(customers.customerByID);

  app.route('/api/customerorderhistory/:customer').all(customersPolicy.isAllowed)
    .get(customers.customerOrderHistory);

  app.route('/api/customerordernodetails/:customerOrderNo').all(customersPolicy.isAllowed)
    .get(customers.customerOrderHistoryByCustOrderNo);

  app.route('/api/customersearchhistory').all(customersPolicy.isAllowed)
    .post(customers.addCustomerSearch);

  app.route('/api/customersearchhistory/:userIdcustomer').all(customersPolicy.isAllowed)
    .get(customers.getCustomerSearchHistoryByUserId);

  app.route('/api/customersearch').all(customersPolicy.isAllowed)
    .get(customers.customerAutocomplete);
};
