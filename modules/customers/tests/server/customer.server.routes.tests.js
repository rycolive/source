'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  customer;

/**
 * Customer routes tests
 */
describe('Customer CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: { data: { userCode: '1JKB' } },
      password: 'password1',
      salesPerson: { data: { CTSTKY: '10KB' } }
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'amelia.rahman@mitrais.com',
      userCode: credentials.username.data.userCode,
      password: credentials.password.toUpperCase(),
      salesPerson: credentials.salesPerson.data.CTSTKY,
      provider: 'local'
    });

    // Save a user to the test db and create new Customer
    user.save(function (err) {
      should.not.exist(err);
      done();
    });
  });

  /* it('should be able to save a Customer if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Customer
        agent.post('/api/customers')
          .send(customer)
          .expect(200)
          .end(function (customerSaveErr, customerSaveRes) {
            // Handle Customer save error
            if (customerSaveErr) {
              return done(customerSaveErr);
            }

            // Get a list of Customers
            agent.get('/api/customers')
              .end(function (customersGetErr, customersGetRes) {
                // Handle Customer save error
                if (customersGetErr) {
                  return done(customersGetErr);
                }

                // Get Customers list
                var customers = customersGetRes.body;

                // Set assertions
                (customers[0].user._id).should.equal(userId);
                (customers[0].name).should.match('Customer name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Customer if not logged in', function (done) {
    agent.post('/api/customers')
      .send(customer)
      .expect(403)
      .end(function (customerSaveErr, customerSaveRes) {
        // Call the assertion callback
        done(customerSaveErr);
      });
  });

  it('should not be able to save an Customer if no name is provided', function (done) {
    // Invalidate name field
    customer.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Customer
        agent.post('/api/customers')
          .send(customer)
          .expect(400)
          .end(function (customerSaveErr, customerSaveRes) {
            // Set message assertion
            (customerSaveRes.body.message).should.match('Please fill Customer name');

            // Handle Customer save error
            done(customerSaveErr);
          });
      });
  });

  it('should be able to update an Customer if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Customer
        agent.post('/api/customers')
          .send(customer)
          .expect(200)
          .end(function (customerSaveErr, customerSaveRes) {
            // Handle Customer save error
            if (customerSaveErr) {
              return done(customerSaveErr);
            }

            // Update Customer name
            customer.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Customer
            agent.put('/api/customers/' + customerSaveRes.body._id)
              .send(customer)
              .expect(200)
              .end(function (customerUpdateErr, customerUpdateRes) {
                // Handle Customer update error
                if (customerUpdateErr) {
                  return done(customerUpdateErr);
                }

                // Set assertions
                (customerUpdateRes.body._id).should.equal(customerSaveRes.body._id);
                (customerUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });*/

  it('should not be able to get a list of Customers if not signed in', function (done) {
    // Request Customers
    agent.get('/api/customers')
      .expect(403)
      .end(function (err, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a list of Customers if signed in', function (done) {
    var _user = {
      firstName: 'newuser',
      lastName: 'test',
      password: 'password1',
      userCode: '1JKB',
      email: 'amelia.rahman@mitrais.com',
      salesPerson: { data: { CTSTKY: '10KB' } }
    };

    agent.post('/api/auth/signup')
      .send(_user)
      .expect(200)
      .end(function (signupErr, signupRes) {
        if (signupErr) {
          return done(signupErr);
        }

        should.not.exist(signupErr);
        agent.post('/api/auth/signin')
          .send(credentials)
          .expect(200)
          .end(function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) {
              return done(signinErr);
            }

            should.not.exist(signinErr);
            // Request Customers
            agent.get('/api/customers')
              .expect(200)
              .end(function (req, res) {
                // Set assertion
                res.body.should.be.instanceof(Array);
                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to get a single Customer if not signed in', function (done) {
    var customerID = '02CASH';
    agent.get('/api/customerbyid/' + customerID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a single Customer if signed in', function (done) {
    var customerID = '02CASH';
    var customerName = 'CASH SALE - SYDNEY';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        agent.get('/api/customerbyid/' + customerID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);
            res.body[0].OKCUNM.should.be.equal(customerName);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should return proper error for single Customer with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    agent.get('/api/customerbyid/test')
      .expect(401)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Customer which doesnt exist, if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        var customerID = '559e9cd815f80b4c256a8f41';

        agent.get('/api/customerbyid/' + customerID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.message.should.equal('Customer with ID ' + customerID + ' is not found');

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get a customer order history if not signed in', function (done) {
    var customerID = '000003';

    agent.get('/api/customerorderhistory/' + customerID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a customer order history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        var customerID = '000003';

        agent.get('/api/customerorderhistory/' + customerID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get a customer search history if not signed in', function (done) {
    agent.get('/api/customersearchhistory/' + credentials.username.data.userCode)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a customer search history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        agent.get('/api/customersearchhistory/' + signinRes.body.user.userCode)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get autocomplete customer if not signed in', function (done) {
    var customerID = '000003';
    agent.get('/api/customersautocomplete/' + customerID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get autocomplete customer if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var customerID = '000003';
        agent.get('/api/customersautocomplete/' + customerID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get an order history details by customer order number if not signed in', function (done) {
    var customerOrder = 'AMEX 2015 SYDNEY';

    agent.get('/api/customerordernodetails/' + customerOrder)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get an order history details by customer order number if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var customerID = '000003';

        // get customer order history by customer id
        agent.get('/api/customerorderhistory/' + customerID)
          .expect(200)
          .end(function (orderHistoryErr, orderHistoryRes) {
            if (orderHistoryErr) {
              return done(orderHistoryErr);
            }

            agent.get('/api/customerordernodetails/' + orderHistoryRes.body[0].CustomerOrder)
              .expect(200)
              .end(function (req, res) {
                // Set assertion
                res.body.should.be.instanceof(Array);
                res.body[0].Order.should.be.equal('0080016288');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to add search history if not signed in', function (done) {
    var customer = {
      'OKCUNO': '000003',
      'OKCUNM': 'MARKETING - MELB',
      'OKSTAT': '20',
      'OKCUA1': '19 WHITEHALL STREET',
      'OKCUA2': 'FOOTSCRAY 3011 VIC',
      'OKCFC1': '1W',
      'OKPHNO': '03 96808000',
      'OKTFNO': '03 96808001',
      'OKTEPY': [
        'N30',
        '30 days net'
      ],
      'OKBLCD': 0,
      'OKCRL3': 0,
      'OKPYCD': 'Chk,all,csh rem',
      'OKTOIN': 0,
      'OKSDST': 'MEL Central',
      'OKSMCD': 'D Simm'
    };

    agent.post('/api/customersearchhistory')
      .send({
        customer: customer,
        user: { userCode: credentials.username.data.userCode }
      })
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to add search history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var customerID = '02CASH';

        // get customer order history by customer id
        agent.get('/api/customerbyid/' + customerID)
          .expect(200)
          .end(function (custErr, custRes) {
            if (custErr) {
              return done(custErr);
            }

            agent.post('/api/customersearchhistory')
              .send({
                customer: custRes.body[0],
                user: { userCode: credentials.username.data.userCode }
              })
              .expect(200)
              .end(function (req, res) {
                // Set assertion
                res.body.affectedRow.should.be.equal(1);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  /* it('should be able to delete an Customer if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Customer
        agent.post('/api/customers')
          .send(customer)
          .expect(200)
          .end(function (customerSaveErr, customerSaveRes) {
            // Handle Customer save error
            if (customerSaveErr) {
              return done(customerSaveErr);
            }

            // Delete an existing Customer
            agent.delete('/api/customers/' + customerSaveRes.body._id)
              .send(customer)
              .expect(200)
              .end(function (customerDeleteErr, customerDeleteRes) {
                // Handle customer error error
                if (customerDeleteErr) {
                  return done(customerDeleteErr);
                }

                // Set assertions
                (customerDeleteRes.body._id).should.equal(customerSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Customer if not signed in', function (done) {
    // Set Customer user
    customer.user = user;

    // Create new Customer model instance
    var customerObj = new Customer(customer);

    // Save the Customer
    customerObj.save(function () {
      // Try deleting Customer
      request(app).delete('/api/customers/' + customerObj._id)
        .expect(403)
        .end(function (customerDeleteErr, customerDeleteRes) {
          // Set message assertion
          (customerDeleteRes.body.message).should.match('User is not authorized');

          // Handle Customer error error
          done(customerDeleteErr);
        });

    });
  });*/

  afterEach(function (done) {
    User.remove().exec(done);
  });
});
