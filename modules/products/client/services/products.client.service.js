// Products service used to communicate Products REST endpoints
(function () {
  'use strict';

  angular
    .module('products')
    .factory('ProductsService', ProductsService)
    .factory('GetProductByIdService', GetProductByIdService)
    .factory('GetProductOrderHistoryService', GetProductOrderHistoryService)
    .factory('GetOrderHistoryByProductOrderNoService', GetOrderHistoryByProductOrderNoService)
    .factory('GetProductStockAvailabilityService', GetProductStockAvailabilityService)
    .factory('ProductSearchHistory', ProductSearchHistory);

  ProductsService.$inject = ['$resource'];
  GetProductByIdService.$inject = ['$resource'];
  GetProductOrderHistoryService.$inject = ['$resource'];
  GetOrderHistoryByProductOrderNoService.$inject = ['$resource'];
  GetProductStockAvailabilityService.$inject = ['$resource'];
  ProductSearchHistory.$inject = ['$resource'];

  function ProductsService($resource) {
    return $resource('api/products/:productId', {
      productId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  function GetProductByIdService($resource) {
    return $resource('api/productbyid/:productId', {
      productId: '@productId'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function GetProductOrderHistoryService($resource) {
    return $resource('api/productorderhistory/:product/:warehouse', {
      product: '@product',
      warehouse: '@warehouse'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function GetOrderHistoryByProductOrderNoService($resource) {
    return $resource('api/productordernodetails/:productOrderNo', {
      productOrderNo: '@productOrderNo'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function GetProductStockAvailabilityService($resource) {
    return $resource('api/productstock/:productStockId/:warehouseId', {
      productStockId: '@productStockId',
      warehouse: '@warehouseId'
    }, {
      get: {
        method: 'GET'
      }
    });
  }

  function ProductSearchHistory($resource) {
    return $resource('api/productsearchhistory/:userCode', {
      userCode: '@userCode'
    }, {
      get: {
        method: 'GET'
      }
    });
  }
}());
