(function () {
  'use strict';

  angular
    .module('products')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('products', {
        abstract: true,
        url: '/products',
        template: '<ui-view/>'
      })
      .state('products.list', {
        url: '',
        templateUrl: 'modules/products/client/views/list-products.client.view.html',
        controller: 'ProductsListController',
        controllerAs: 'vm',
        resolve: {
          productResolve: newProduct
        },
        data: {
          pageTitle: 'Products List'
        },
        breadcrumb: {
          label: 'Products List',
          parent: 'home'
        }
      })
      .state('products.productdetails', {
        url: '/:productId',
        templateUrl: 'modules/products/client/views/list-products.client.view.html',
        controller: 'ProductsListController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },
        data: {
          pageTitle: 'Products List'
        },
        breadcrumb: {
          label: 'Products List',
          parent: 'home'
        }
      })
      .state('products.create', {
        url: '/create',
        templateUrl: 'modules/products/client/views/form-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: newProduct
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Products Create'
        }
      })
      .state('products.edit', {
        url: '/:productId/edit',
        templateUrl: 'modules/products/client/views/form-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Product {{ productResolve.name }}'
        }
      })
      /* .state('products.view', {
        url: '/details/:productId',
        templateUrl: 'modules/products/client/views/view-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },
        data: {
          pageTitle: 'Product {{ productResolve.name }}'
        },
        breadcrumb: {
          label: 'Product Detail',
          parent: 'products.list'
        }
      })*/
      .state('products.saleshistory', {
        url: '/saleshistory/:productOrderNo',
        templateUrl: 'modules/products/client/views/view-product-order-history-details.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          orderhistoryResolve: getOrderHistory
        },
        data: {
          pageTitle: 'Product Sales Details'
        },
        breadcrumb: {
          label: 'Product Sales History Details',
          parent: 'products.list'
        }
      })
      .state('products.searchhistory', {
        url: '/search/history',
        templateUrl: 'modules/products/client/views/list-products-search-history.client.view.html',
        controller: 'ProductsSearchHistoryController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Product Search History'
        },
        breadcrumb: {
          label: 'Product Search History',
          parent: 'products.list'
        }
      });
  }

  getProduct.$inject = ['$stateParams', 'GetProductByIdService'];

  function getProduct($stateParams, GetProductByIdService) {
    return GetProductByIdService.query({
      productId: $stateParams.productId
    }).$promise;
  }

  newProduct.$inject = ['ProductsService'];

  function newProduct(ProductsService) {
    return new ProductsService();
  }

  getOrderHistory.$inject = ['$stateParams', 'GetOrderHistoryByProductOrderNoService'];

  function getOrderHistory($stateParams, GetOrderHistoryByProductOrderNoService) {
    return GetOrderHistoryByProductOrderNoService.query({
      productOrderNo: $stateParams.productOrderNo
    }).$promise;
  }
}());
