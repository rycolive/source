(function () {
  'use strict';

  // Products controller
  angular
    .module('products')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['$scope', '$state', 'Authentication', 'orderhistoryResolve'];

  function ProductsController ($scope, $state, Authentication, order) {
    var vm = this;

    vm.authentication = Authentication;

    // display order summary if not empty
    if (order !== undefined) {
      vm.order = order;
    }
  }
}());
