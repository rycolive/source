(function () {
  'use strict';

  angular
    .module('products')
    .controller('ProductsListController', ProductsListController);

  ProductsListController.$inject = ['ProductsService', 'productResolve', '$scope', '$filter', '$window', '$http', '$cookies', 'GetProductByIdService', 'GetProductStockAvailabilityService', 'GetProductOrderHistoryService', 'Authentication'];

  function ProductsListController(ProductsService, product, $scope, $filter, $window, $http, $cookies, GetProductByIdService, GetProductStockAvailabilityService, GetProductOrderHistoryService, Authentication) {
    var vm = this;
    vm.authentication = Authentication;
    vm.dataLoad = true;
    vm.dataLoadHistory = false;
    vm.dataLoadStock = false;
    vm.displayProductDetails = false;

    // gets products data from IVOS_app_JQuey_Item
    /* ProductsService.query(function(data) {
      vm.products = data;

      if (product.$promise !== undefined) {
        // sets search box value
        $scope.item = $filter('filter')(vm.products, {
          MMITNO: product[0].MMITNO,
          MMITDS: product[0].MMITDS
        })[0];

        // load product details
        $scope.getProductDetails(product[0]);
      }

      vm.dataLoad = true;
    });*/

    $scope.getProductSearchList = function (searchString) {
      // call the search
      return $http.get('/api/productsearch', {
        params: {
          searchString: searchString
        }
      }).then(function(response) {
        vm.dataLoad = true;
        return response.data;
      });
    };

    // gets product details
    $scope.getProductDetails = function (product) {
      vm.displayProductDetails = true;
      GetProductByIdService.query({ productId: product.MMITNO }, function(data) {
        vm.product = data[0];

        // get product sales history
        GetProductOrderHistoryService.query({
          product: vm.product.MMITNO,
          warehouse: vm.authentication.warehouse
        }, function(data) {
          $scope.item = '';
          vm.orderHistory = data;
          vm.buildPagerHistory();
        }, function (error) {
          vm.error = error.message;
        });

        // get product stock
        GetProductStockAvailabilityService.query({
          productStockId: vm.product.MMITNO,
          warehouseId: vm.authentication.warehouse
        }, function(data) {
          vm.productStock = data;
          vm.buildPagerStock();

          // insert search data into IVOS_app_productsearch_history
          $http.post('/api/productsearchhistory', {
            product: vm.product,
            user: vm.authentication.user,
            stock: $filter('filter')(vm.productStock, { WH: $cookies.getObject('R247WH').MWWHLO })
          }).success(function(response) {
            console.log('success');
          }).error(function(err) {
            console.log(err);
          });
        }, function (error) {
          vm.error = error.message;
        });
      });
    };

    if (product.$promise !== undefined) {
      if ($scope.item == null) {
        $scope.item = '';
      }

      // load product details
      $scope.getProductDetails(product[0]);
      vm.dataLoad = true;
    }

    // build pager product sales history
    vm.buildPagerHistory = function () {
      vm.pagedHistoryItems = [];
      vm.itemsPerPage = 5;
      vm.currentHistoryPage = 1;
      vm.maxSize = 10;
      vm.figureOutHistoryItemsToDisplay();
    };

    // filtering product sales history
    vm.figureOutHistoryItemsToDisplay = function () {
      vm.filteredItems = $filter('filter')(vm.orderHistory);
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentHistoryPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedHistoryItems = vm.filteredItems.slice(begin, end);
      vm.dataLoadHistory = true;
    };

    // event page changed product sales history
    vm.pageHistoryChanged = function () {
      vm.figureOutHistoryItemsToDisplay();
    };

    // build pager product stock
    vm.buildPagerStock = function () {
      vm.pagedStockItems = [];
      vm.itemsPerPage = 5;
      vm.currentStockPage = 1;
      vm.maxSize = 10;
      vm.figureOutStockItemsToDisplay();
    };

    // filtering product stock
    vm.figureOutStockItemsToDisplay = function () {
      vm.filteredStockItems = $filter('filter')(vm.productStock);
      vm.filterStockLength = vm.filteredStockItems.length;
      var begin = ((vm.currentStockPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedStockItems = vm.filteredStockItems.slice(begin, end);
      vm.dataLoadStock = true;
    };

    // event page changed product stock
    vm.pageStockChanged = function () {
      vm.figureOutStockItemsToDisplay();
    };

    // set focus on search box
    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      var element = $window.document.getElementById('productSearch');
      if (element && !element.activeElement) {
        element.focus();
      }
    }
  }
}());
