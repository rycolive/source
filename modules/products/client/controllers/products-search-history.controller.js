(function () {
  'use strict';

  // Products Search History controller
  angular
    .module('products')
    .controller('ProductsSearchHistoryController', ProductsSearchHistoryController);

  ProductsSearchHistoryController.$inject = ['$scope', '$state', '$filter', 'Authentication', 'ProductSearchHistory'];

  function ProductsSearchHistoryController ($scope, $state, $filter, Authentication, ProductSearchHistory) {
    var vm = this;
    vm.authentication = Authentication;
    vm.dataLoad = false;

    // gets product search history from IVOS_app_productsearch_history
    ProductSearchHistory.query({ userCode: vm.authentication.user.userCode }, function(data) {
      vm.searchHistory = data;
      vm.buildPager();
    });

    // build pager for product search history table
    vm.buildPager = function () {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;
      vm.maxSize = 10;
      vm.figureOutItemsToDisplay();
    };

    // display items in table
    vm.figureOutItemsToDisplay = function () {
      vm.filteredItems = $filter('filter')(vm.searchHistory, {
        $: vm.search
      });
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
      vm.dataLoad = true;
    };

    // event on page changed
    vm.pageChanged = function () {
      vm.figureOutItemsToDisplay();
    };
  }
}());
