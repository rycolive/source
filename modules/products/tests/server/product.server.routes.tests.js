'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  product;

/**
 * Product routes tests
 */
describe.only('Product CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: { data: { userCode: '1JKB' } },
      password: 'password1',
      salesPerson: { data: { CTSTKY: '10KB' } }
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'amelia.rahman@mitrais.com',
      userCode: credentials.username.data.userCode,
      password: credentials.password.toUpperCase(),
      salesPerson: credentials.salesPerson.data.CTSTKY,
      provider: 'local'
    });

    // Save a user to the test db and create new Product
    user.save(function (err) {
      should.not.exist(err);
      done();
    });
  });

  /* it('should be able to save a Product if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Product
        agent.post('/api/products')
          .send(product)
          .expect(200)
          .end(function (productSaveErr, productSaveRes) {
            // Handle Product save error
            if (productSaveErr) {
              return done(productSaveErr);
            }

            // Get a list of Products
            agent.get('/api/products')
              .end(function (productsGetErr, productsGetRes) {
                // Handle Product save error
                if (productsGetErr) {
                  return done(productsGetErr);
                }

                // Get Products list
                var products = productsGetRes.body;

                // Set assertions
                (products[0].user._id).should.equal(userId);
                (products[0].name).should.match('Product name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Product if not logged in', function (done) {
    agent.post('/api/products')
      .send(product)
      .expect(403)
      .end(function (productSaveErr, productSaveRes) {
        // Call the assertion callback
        done(productSaveErr);
      });
  });

  it('should not be able to save an Product if no name is provided', function (done) {
    // Invalidate name field
    product.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Product
        agent.post('/api/products')
          .send(product)
          .expect(400)
          .end(function (productSaveErr, productSaveRes) {
            // Set message assertion
            (productSaveRes.body.message).should.match('Please fill Product name');

            // Handle Product save error
            done(productSaveErr);
          });
      });
  });

  it('should be able to update an Product if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Product
        agent.post('/api/products')
          .send(product)
          .expect(200)
          .end(function (productSaveErr, productSaveRes) {
            // Handle Product save error
            if (productSaveErr) {
              return done(productSaveErr);
            }

            // Update Product name
            product.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Product
            agent.put('/api/products/' + productSaveRes.body._id)
              .send(product)
              .expect(200)
              .end(function (productUpdateErr, productUpdateRes) {
                // Handle Product update error
                if (productUpdateErr) {
                  return done(productUpdateErr);
                }

                // Set assertions
                (productUpdateRes.body._id).should.equal(productSaveRes.body._id);
                (productUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });*/

  it('should not be able to get a list of Products if not signed in', function (done) {
    // Request Products
    agent.get('/api/products')
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a list of Products if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        // Request Products
        agent.get('/api/products')
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get a single Product if not signed in', function (done) {
    var productID = '750';
    agent.get('/api/productbyid/' + productID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Product with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    agent.get('/api/productbyid/test')
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Product which doesnt exist, if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        var productID = '559e9cd815f80b4c256a8f41';

        agent.get('/api/productbyid/' + productID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.message.should.equal('Product with ID ' + productID + ' is not found');

            // Call the assertion callback
            done();
          });
      });
  });

  it('should be able to get a single Product if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        var productID = '750';
        var productName = '150MM SPRING GUARD FOR TJ24';

        agent.get('/api/productbyid/' + productID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);
            res.body[0].MMITNO.should.be.equal(productID);
            res.body[0].MMITDS.should.be.equal(productName);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get a product order history if not signed in', function (done) {
    var productID = '750';
    var warehouseID = '044';

    agent.get('/api/productorderhistory/' + productID + '/' + warehouseID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a product order history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);
        var productID = '750';
        var warehouseID = '044';

        agent.get('/api/productorderhistory/' + productID + '/' + warehouseID)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get a product search history if not signed in', function (done) {
    agent.get('/api/productsearchhistory/' + credentials.username.data.userCode)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get a product search history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        agent.get('/api/productsearchhistory/' + signinRes.body.user.userCode)
          .expect(200)
          .end(function (req, res) {
            // Set assertion
            res.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to get an order history details by product order number if not signed in', function (done) {
    var productOrder = '0010359913';

    agent.get('/api/productordernodetails/' + productOrder)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get an order history details by product order number if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var productID = 'T6004D';
        var warehouseID = '044';

        // get customer order history by customer id
        agent.get('/api/productorderhistory/' + productID + '/' + warehouseID)
          .expect(200)
          .end(function (orderHistoryErr, orderHistoryRes) {
            if (orderHistoryErr) {
              return done(orderHistoryErr);
            }

            agent.get('/api/productordernodetails/' + orderHistoryRes.body[0].Order)
              .expect(200)
              .end(function (req, res) {
                // Set assertion
                res.body.should.be.instanceof(Array);
                res.body[0].Order.should.be.equal('0010359913');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to get product stock availability if not signed in', function (done) {
    var productID = 'HAT6004D';
    var warehouseID = '044';

    agent.get('/api/productstock/' + productID + '/' + warehouseID)
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to get product stock availability if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var productID = 'T6004D';
        var warehouseID = '044';

        // get product stock product id
        agent.get('/api/productstock/' + productID + '/' + warehouseID)
          .expect(200)
          .end(function (stockErr, stockRes) {
            if (stockErr) {
              return done(stockErr);
            }

            stockRes.body.should.be.instanceof(Array);

            // Call the assertion callback
            done();
          });
      });
  });

  it('should not be able to add search history if not signed in', function (done) {
    var product = {
      'MMITNO': '750',
      'MMITDS': '150MM SPRING GUARD FOR TJ24',
      'MMSTAT': '20',
      'MMUNMS': 'EA',
      'MMNEWE': 69,
      'PRICE1R': 4.2,
      'PRICE1W': 3.25
    };

    var productStock = [{
      WH: '044',
      Desc: 'BMA Blackwater',
      Item: 'HAT6004D',
      Status: '20',
      Allocatable: 0
    }];

    agent.post('/api/productsearchhistory')
      .send({
        product: product,
        user: { userCode: credentials.username.data.userCode },
        stock: productStock
      })
      .expect(403)
      .end(function (req, res) {
        // Set assertion
        res.body.message.should.equal('User is not authorized');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to add search history if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        should.not.exist(signinErr);

        var productID = 'HAT6004D';

        // get product order history by product id
        agent.get('/api/productbyid/' + productID)
          .expect(200)
          .end(function (prodErr, prodRes) {
            if (prodErr) {
              return done(prodErr);
            }

            var warehouseID = '044';
            var productStock = [{
              WH: '044',
              Desc: 'BMA Blackwater',
              Item: 'HAT6004D',
              Status: '20',
              Allocatable: 0
            }];

            agent.post('/api/productsearchhistory')
              .send({
                product: prodRes.body[0],
                user: { userCode: credentials.username.data.userCode },
                stock: productStock
              })
              .expect(200)
              .end(function (req, res) {
                console.log(res.body);
                // Set assertion
                res.body.affectedRow.should.be.equal(1);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  /* it('should be able to delete an Product if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Product
        agent.post('/api/products')
          .send(product)
          .expect(200)
          .end(function (productSaveErr, productSaveRes) {
            // Handle Product save error
            if (productSaveErr) {
              return done(productSaveErr);
            }

            // Delete an existing Product
            agent.delete('/api/products/' + productSaveRes.body._id)
              .send(product)
              .expect(200)
              .end(function (productDeleteErr, productDeleteRes) {
                // Handle product error error
                if (productDeleteErr) {
                  return done(productDeleteErr);
                }

                // Set assertions
                (productDeleteRes.body._id).should.equal(productSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Product if not signed in', function (done) {
    // Set Product user
    product.user = user;

    // Create new Product model instance
    var productObj = new Product(product);

    // Save the Product
    productObj.save(function () {
      // Try deleting Product
      request(app).delete('/api/products/' + productObj._id)
        .expect(403)
        .end(function (productDeleteErr, productDeleteRes) {
          // Set message assertion
          (productDeleteRes.body.message).should.match('User is not authorized');

          // Handle Product error error
          done(productDeleteErr);
        });

    });
  });*/

  afterEach(function (done) {
    User.remove().exec(done);
  });
});
