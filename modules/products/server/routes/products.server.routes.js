'use strict';

/**
 * Module dependencies
 */
var productsPolicy = require('../policies/products.server.policy'),
  products = require('../controllers/products.server.controller');

module.exports = function(app) {
  // Products Routes
  app.route('/api/products').all(productsPolicy.isAllowed)
    .get(products.list);
    // .post(products.create);

  /* app.route('/api/products/:productId').all(productsPolicy.isAllowed)
    .get(products.read)
    .put(products.update)
    .delete(products.delete);*/

  app.route('/api/productbyid/:productId').all(productsPolicy.isAllowed)
    .get(products.productByID);

  app.route('/api/productorderhistory/:product/:warehouse').all(productsPolicy.isAllowed)
    .get(products.productOrderHistory);

  app.route('/api/productstock/:productStockId/:warehouseId').all(productsPolicy.isAllowed)
    .get(products.productStockByProdID);

  app.route('/api/productordernodetails/:productOrderNo').all(productsPolicy.isAllowed)
    .get(products.productOrderHistoryByProdOrderNo);

  app.route('/api/productsearchhistory').all(productsPolicy.isAllowed)
    .post(products.addProductSearch);

  app.route('/api/productsearchhistory/:userCode').all(productsPolicy.isAllowed)
    .get(products.getProductSearchHistoryByUserId);

  app.route('/api/productsearch').all(productsPolicy.isAllowed)
    .get(products.productAutocomplete);
};
