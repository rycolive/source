'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Products Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/products',
      permissions: '*'
    }, {
      resources: '/api/products/:productId',
      permissions: '*'
    }, {
      resources: '/api/productsearchhistory',
      permissions: '*'
    }, {
      resources: '/api/productbyid/:productId',
      permissions: '*'
    }, {
      resources: '/api/productorderhistory/:product/:warehouse',
      permissions: '*'
    }, {
      resources: '/api/productstock/:productStockId/:warehouseId',
      permissions: '*'
    }, {
      resources: '/api/productordernodetails/:productOrderNo',
      permissions: '*'
    }, {
      resources: '/api/productsearchhistory/:userCode',
      permissions: '*'
    }, {
      resources: '/api/productsearch',
      permissions: ['get']
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/products',
      permissions: ['get', 'post']
    }, {
      resources: '/api/products/:productId',
      permissions: ['get']
    }, {
      resources: '/api/productsearchhistory',
      permissions: ['post']
    }, {
      resources: '/api/productbyid/:productId',
      permissions: ['get']
    }, {
      resources: '/api/productorderhistory/:product/:warehouse',
      permissions: ['get']
    }, {
      resources: '/api/productstock/:productStockId/:warehouseId',
      permissions: ['get']
    }, {
      resources: '/api/productordernodetails/:productOrderNo',
      permissions: ['get']
    }, {
      resources: '/api/productsearchhistory/:userCode',
      permissions: ['get']
    }, {
      resources: '/api/productsearch',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Products Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? ['user'] : ['guest'];

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
