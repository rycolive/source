'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Product = mongoose.model('Product'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');
var sql = require('mssql');
var config = require(path.resolve('./config/config'));
var async = require('async');
var fs = require('fs');

/**
 * Create a Product
 */
/* exports.create = function(req, res) {
  var product = new Product(req.body);
  product.user = req.user;

  product.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });
};*/

/**
 * Show the current Product
 */
/* exports.read = function(req, res) {
  // convert mongoose document to JSON
  var product = req.product ? req.product.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  product.isCurrentUserOwner = req.user && product.user && product.user._id.toString() === req.user._id.toString();

  res.jsonp(product);
};*/

/**
 * Update a Product
 */
/* exports.update = function(req, res) {
  var product = req.product;

  product = _.extend(product, req.body);

  product.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });
};*/

/**
 * Delete an Product
 */
/* exports.delete = function(req, res) {
  var product = req.product;

  product.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });
};*/

/**
 * List of Products
 */
exports.list = function(req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/GetAll_Products.sql').toString();
  var connection = new sql.Connection(config.mssqldb_cat, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

/**
 * Product middleware
 */
exports.productByID = function(req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductById.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('productId', sql.VarChar(50), req.params.productId);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else if (recordset.length !== 0) {
          res.json(recordset);
        } else {
          res.status(400).send({
            message: 'Product with ID ' + req.params.productId + ' is not found'
          });
        }
      });
  });
};

exports.productAutocomplete = function(req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductAutocomplete.sql').toString();
  var connection = new sql.Connection(config.mssqldb_cat, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('searchString', sql.VarChar(50), req.query.searchString);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.jsonp(recordset);
        }
      });
  });
};

exports.productOrderHistory = function(req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductOrderHistoryByProductId.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('productId', sql.VarChar(50), req.params.product);
    request.input('warehouseId', sql.VarChar(50), req.params.warehouse);
    request.input('companyId', sql.VarChar(50), '100');
    request.input('orderType', sql.VarChar(50), 'R19');

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err);
        } else {
          res.json(recordset);
        }
      });
  });
};

exports.productOrderHistoryByProdOrderNo = function (req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductOrderHistoryByProdOrderNo.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('productOrderNo', sql.VarChar(50), req.params.productOrderNo);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

exports.productStockByProdID = function(req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductStockAvailability.sql').toString();
  var connection = new sql.Connection(config.mssqldb_m3, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('productId', sql.VarChar(50), req.params.productStockId);
    request.input('warehouseId', sql.VarChar(50), req.params.warehouseId);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err);
        } else {
          res.json(recordset);
        }
      });
  });
};

exports.addProductSearch = function (req, res) {
  var connection = new sql.Connection(config.mssqldb_application, function (err) {
    if (err) {
      return res.send(err.message);
    }

    // check if search history is exists, if yes, then update the registered date
    var checkHistoryQuerySQL = fs.readFileSync('./modules/products/server/sql/Get_ProductSearch_History_ByProdId_UserId.sql').toString();
    var getHistoryRequest = new sql.Request(connection);

    getHistoryRequest.input('productId', req.body.product.MMITNO);
    getHistoryRequest.input('userCode', req.body.user.userCode);

    // get product search history record by product and registerdby
    getHistoryRequest.query(checkHistoryQuerySQL, function(err, historyRecord) {
      if (err) {
        console.log(err);
        return res.send(err.message);
      } else {
        // check if historyRecord is exists
        if (historyRecord.length !== 0) {
          // history record is exists
          // update registered history record
          var updateHistorySQL = fs.readFileSync('./modules/products/server/sql/Update_ProductSearch_History.sql').toString();

          // sql transaction
          var transactionUpdate = connection.transaction();
          transactionUpdate.begin(function(err) {
            if (err) {
              console.log('Begin Error: ' + err);
            }

            var inserted = 0;
            var rolledBack = false;

            // define transaction rollback function
            transactionUpdate.on('rollback', function(aborted) {
              console.log('rollback at update method');
              console.log(aborted);
              rolledBack = true;
            });

            var updateRequest = new sql.Request(transactionUpdate);

            // sql params product, registered, registeredby
            updateRequest.input('productId', sql.VarChar(50), req.body.product.MMITNO);
            updateRequest.input('productDesc', sql.VarChar(50), req.body.product.MMITDS);
            updateRequest.input('status', sql.VarChar(50), req.body.product.MMSTAT);
            updateRequest.input('uom', sql.VarChar(50), req.body.product.MMUNMS);
            updateRequest.input('priceR1', sql.Numeric(17, 2), req.body.product.PRICE1R);
            updateRequest.input('priceW1', sql.Numeric(17, 2), req.body.product.PRICE1W);
            updateRequest.input('availability', sql.Numeric(17, 2), req.body.stock[0] ? req.body.stock[0].Allocatable : 0);
            updateRequest.input('registered', sql.DateTime2, new Date());
            updateRequest.input('registeredBy', sql.VarChar(50), req.body.user.userCode);

            // update Registered on IVOS_app_productsearch_history filtered by product and registeredby
            updateRequest.query(updateHistorySQL,
            function(err, recordset, affectedRow) {
              if (err) {
                if (!rolledBack) {
                  transactionUpdate.rollback(function(err) {
                    console.log('==================== rolled back update transaction ======================');
                    console.log(err);
                  });
                }

                // update sql database failed
                console.log(err);
                return res.send(err);
              } else {
                transactionUpdate.commit(function(err, recordset) {
                  if (err) {
                    if (!rolledBack) {
                      transactionUpdate.rollback(function(err) {
                        console.log('==================== rolled back update transaction ======================');
                        console.log(err);
                      });
                    }
                  }

                  console.log('Transaction committed. IVOS_app_productsearch_history updated');
                });

                res.send({ affectedRow: affectedRow });
              }
            });
          });
        } else {
          // history record not found
          // insert new history record
          var querysql = fs.readFileSync('./modules/products/server/sql/Insert_ProductSearch_History.sql').toString();

          // sql transaction
          var transaction = connection.transaction();
          transaction.begin(function(err) {
            if (err) {
              console.log('Begin Error: ' + err);
            }

            var inserted = 0;
            var rolledBack = false;

            // define transaction rollback function
            transaction.on('rollback', function(aborted) {
              console.log('rollback at insert method');
              console.log(aborted);
              rolledBack = true;
            });

            var request = new sql.Request(transaction);

            // sql params
            request.input('productId', sql.VarChar(50), req.body.product.MMITNO);
            request.input('productDesc', sql.VarChar(50), req.body.product.MMITDS);
            request.input('status', sql.VarChar(50), req.body.product.MMSTAT);
            request.input('uom', sql.VarChar(50), req.body.product.MMUNMS);
            request.input('priceR1', sql.Numeric(17, 2), req.body.product.PRICE1R);
            request.input('priceW1', sql.Numeric(17, 2), req.body.product.PRICE1W);
            request.input('availability', sql.Numeric(17, 2), req.body.stock.length !== 0 ? req.body.stock[0].Allocatable : 0);
            request.input('registered', sql.DateTime2, new Date());
            request.input('registeredBy', sql.VarChar(50), req.body.user.userCode);

            console.log('sampe');

            // insert into IVOS_app_productsearch_history
            request.query(querysql,
            function(err, recordset, affectedRow) {
              console.log(affectedRow);
              if (err) {
                if (!rolledBack) {
                  transaction.rollback(function(err) {
                    console.log('==================== rolled back ======================');
                    console.log(err);
                  });
                }
                console.log(err);
                console.log('==================== insert into sql database failed ======================');
                return res.send(err);
              } else {
                transaction.commit(function(err, recordset) {
                  if (err) {
                    if (!rolledBack) {
                      transaction.rollback(function(err) {
                        console.log('==================== rolled back ======================');
                        console.log(err);
                      });
                    }
                  }

                  console.log('SQL Transaction committed.');
                });

                res.send({ affectedRow: affectedRow });
              }
            });
          });
        }
      }
    });
  });
};

exports.getProductSearchHistoryByUserId = function (req, res) {
  var querysql = fs.readFileSync('./modules/products/server/sql/Get_ProductSearch_History_ByUserId.sql').toString();
  var connection = new sql.Connection(config.mssqldb_application, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('userCode', sql.VarChar(50), req.params.userCode);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};
