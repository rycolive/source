INSERT INTO
	IVOS_app_productsearch_history
VALUES
	(
		@productId,
		@productDesc,
		@status,
		@uom,
		@priceR1,
		@priceW1,
		@availability,
		@registered,
		@registeredBy
	)