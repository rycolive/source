UPDATE
	IVOS_app_productsearch_history
SET
	Registered = @Registered,
	Description = @productDesc,
	Status = @status,
	UOM = @uom,
	ListPrice1R = @priceR1,
	ListPrice1W = @priceW1,
	Availability = @availability
WHERE
	RegisteredBy = @RegisteredBy
AND
	Item = @productId