SELECT
	Item,
	Description,
	Status,
	UOM,
	ListPrice1R,
	ListPrice1W,
	Availability,
	Registered,
	RegisteredBy
FROM
	IVOS_app_productsearch_history
WHERE
	RegisteredBy = @userCode
AND
	Item = @productId
ORDER BY
	Registered DESC