SELECT
	OBITNO, 
	OBORNO AS 'Order', 
	OACUNO AS 'Customer', 
	convert(datetime, convert(varchar, OAORDT)) AS 'OrderDate', 
	OBORQT AS 'Qty', 
	OBNEPR AS 'Price', 
	OBNEPR * OBORQT AS 'NetTotal'
FROM 
	OOHEAD, 
	OOLINE
WHERE 
	OOHEAD.OAORNO = OOLINE.OBORNO
AND 
	OBORNO = @productOrderNo
ORDER BY 
	OAORDT DESC
