(function () {
  'use strict';

  angular
    .module('core')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$state', '$scope', 'Authentication', '$window'];

  function HomeController($state, $scope, Authentication, $window) {
    var vm = this;
    vm.authentication = Authentication;

    if (!vm.authentication.user) {
      $state.go('authentication.signin');
    }
  }
}());
