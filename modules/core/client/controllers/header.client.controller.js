(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', '$window', '$cookies', 'Authentication', 'menuService'];

  function HeaderController($scope, $state, $window, $cookies, Authentication, menuService) {
    var vm = this;

    vm.accountMenu = menuService.getMenu('account').items[0];
    vm.authentication = Authentication;
    vm.isCollapsed = false;
    vm.menu = menuService.getMenu('topbar');

    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      // Collapsing the menu after navigation
      vm.isCollapsed = false;

      if ($cookies.getObject('R247WH') !== undefined) {
        vm.warehouse = $cookies.getObject('R247WH').MWWHLO;
      }
    }
  }
}());
