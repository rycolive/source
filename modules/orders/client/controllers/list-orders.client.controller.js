(function () {
  'use strict';

  angular
    .module('orders')
    .controller('OrdersListController', OrdersListController);

  OrdersListController.$inject = ['OrdersService', 'Authentication', '$window', '$cookies'];

  function OrdersListController(OrdersService, Authentication, $window, $cookies) {
    var vm = this;
    vm.authentication = Authentication;

    // set warehouse variable from cookies
    if ($cookies.getObject('R247WH') !== undefined) {
      vm.warehouse = $cookies.getObject('R247WH').MWWHLO;
    }

    vm.nonbma = OrdersService.query();
    vm.ha = [{
      id: 'a',
      status: 'a',
      sales: 'a',
      warehouse: vm.warehouse,
      customer: 'a',
      date: 'a',
      name: 'a',
      user: 'a' }];
    vm.cm = [{
      id: 'a',
      status: 'a',
      sales: 'a',
      warehouse: vm.warehouse,
      customer: 'a',
      date: 'a',
      name: 'a',
      user: 'a',
      total: 'a',
      type: 'a' }];
  }
}());
