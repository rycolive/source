(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('UsersService', UsersService);

  UsersService.$inject = ['$resource'];

  function UsersService($resource) {
    return $resource('api/users', {}, {
      update: {
        method: 'PUT'
      }
    });
  }

  // TODO this should be Users service
  angular
    .module('users.admin.services')
    .factory('AdminService', AdminService)
    .factory('GetUserBySalesPersonService', GetUserBySalesPersonService);

  AdminService.$inject = ['$resource'];
  GetUserBySalesPersonService.$inject = ['$resource', '$q'];

  function AdminService($resource) {
    return $resource('api/users/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  function GetUserBySalesPersonService($resource) {
    return $resource('api/userbysalesperson/:salesPersonId', {
      salesPersonId: '@salesPersonId'
    }, {
      get: {
        method: 'GET',
        isArray: true
      }
    });
  }
}());
