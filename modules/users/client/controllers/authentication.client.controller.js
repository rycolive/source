(function () {
  'use strict';

  angular
    .module('users')
    .controller('AuthenticationController', AuthenticationController);

  AuthenticationController.$inject = ['$scope', '$state', '$http', '$location', '$window', '$cookies', 'Authentication', 'PasswordValidator', 'SalespeopleService', 'WarehousesService', 'GetUserBySalesPersonService'];

  function AuthenticationController($scope, $state, $http, $location, $window, $cookies, Authentication, PasswordValidator, SalespeopleService, WarehousesService, GetUserBySalesPersonService) {
    var vm = this;
    vm.authentication = Authentication;
    vm.state = $state;
    vm.getPopoverMsg = PasswordValidator.getPopoverMsg;
    vm.signup = signup;
    vm.signin = signin;
    vm.callOauthProvider = callOauthProvider;
    vm.salesPerson = SalespeopleService.query();
    vm.warehouse = WarehousesService.query();
    vm.credentials = {};

    vm.getUserList = function (salesPersonData) {
      // clear the selected before overwrite the combobox
      vm.credentials.username = {};
      vm.credentials.username.data = undefined;

      var salesPersonId = salesPersonData.CTSTKY;

      // call admin service get user by salesperson
      GetUserBySalesPersonService.get({ salesPersonId: salesPersonId }, function (data) {
        vm.username = data;
      });
    };

    // set the cookies (salesperson, warehouse, user) on the fields
    if ($cookies.getObject('R247SP') !== undefined) {
      vm.credentials.salesPerson = {};
      vm.credentials.salesPerson.data = $cookies.getObject('R247SP');
      vm.salesPerson.data = $cookies.getObject('R247SP');
      vm.getUserList(vm.salesPerson.data);
    }

    if ($cookies.getObject('R247WH') !== undefined) {
      vm.warehouse.data = $cookies.getObject('R247WH');
    }

    if ($cookies.getObject('R247U') !== undefined) {
      vm.credentials.username = {};
      vm.credentials.username.data = $cookies.getObject('R247U');
    }

    // Get an eventual error defined in the URL query string:
    vm.error = $location.search().err;

    // If user is signed in then redirect back home
    if (vm.authentication.user) {
      $location.path('/');
    }

    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 365);

    function signup(isValid) {
      vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');

        return false;
      }

      vm.credentials.passwordReset = 'No';
      $http.post('/api/auth/signup', vm.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        vm.authentication.user = response.user;
        vm.authentication.warehouse = response.warehouse;
        $cookies.putObject('R247SP', vm.salesPerson.data, { expires: expireDate });
        $cookies.putObject('R247U', vm.credentials.username.data, { expires: expireDate });
        $cookies.putObject('R247WH', vm.warehouse.data, { expires: expireDate });

        // And redirect to the previous or home page
        console.log($state.previous.params);
        $state.go($state.previous.state.name || 'home', { user: $state.previous.params, warehouse: response.warehouse });
      }).error(function (response) {
        vm.error = response.message;
        console.log(response);
      });
    }

    function signin(isValid) {
      vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');

        return false;
      }

      vm.credentials.warehouse = vm.warehouse.data.MWWHLO;
      $http.post('/api/auth/signin', vm.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        vm.authentication.user = response.user;
        vm.authentication.warehouse = response.warehouse;
        $cookies.putObject('R247SP', vm.salesPerson.data, { expires: expireDate });
        $cookies.putObject('R247U', vm.credentials.username.data, { expires: expireDate });
        $cookies.putObject('R247WH', vm.warehouse.data, { expires: expireDate });

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        vm.error = response;
      });
    }

    // OAuth provider request
    function callOauthProvider(url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    }
  }
}());
