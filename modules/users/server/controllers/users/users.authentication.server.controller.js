'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  passport = require('passport'),
  User = mongoose.model('User');
var config = require(path.resolve('./config/config'));
var sql = require('mssql');
var fs = require('fs');
var crypto = require('crypto');

// URLs for which user can't be redirected on signin
var noReturnUrls = [
  '/authentication/signin',
  '/authentication/signup'
];

/**
 * Signup
 */
exports.signup = function (req, res) {
  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;

  // insert into sql database then insert into mongodb
  // connect to SQL RHAU_Application db
  var connection = new sql.Connection(config.mssqldb_application, function(err) {
    if (err) {
      return res.send(err.message);
    }

    console.log('==================== connected to sql database ======================');
    var request = new sql.Request(connection);
    var querysql = fs.readFileSync('./modules/users/server/sql/Get_LastUserBySalesPerson.sql').toString();
    var userCode = '';

    // SQL params
    request.input('salesPerson', sql.VarChar(50), req.body.salesPerson.data.CTSTKY);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          console.log('==================== get last user failed ======================');
          return res.send(err.message);
        }

        console.log('==================== successfully get last user ======================');
        console.log(recordset);

        // if user exists continue the increment, if not, start from 1A[last 2 chars at salesPersonCode], ex: 1AKB
        if (recordset.length !== 0) {
          // create usercode
          var userNumber = recordset[0].userCode.substring(0, 1);
          var userChar = recordset[0].userCode.substring(1, 2);
          var salesPersonCode = req.body.salesPerson.data.CTSTKY.substring(req.body.salesPerson.data.CTSTKY.length - 2);
          var userpassword = crypto.createHash('sha1').update('m3J7d9g5W8c0X6vC3wHG6d8n3' + req.body.password.toUpperCase() + '5R4s8CK3h87c3nm9d0PK83vT4', 'utf8').digest('hex');

          // userNumber and userChar increment
          if (userChar.toLowerCase() === 'z') {
            userNumber += 1;
          } else {
            userChar = String.fromCharCode(userChar.charCodeAt(0) + 1);
          }

          // new usercode
          userCode = userNumber + userChar + salesPersonCode;

          // begin transaction
          var transaction = connection.transaction();
          transaction.begin(function(err) {
            if (err) {
              console.log('Begin Error: ' + err);
            }

            var inserted = 0;
            var rolledBack = false;

            // define transaction rollback function
            transaction.on('rollback', function(aborted) {
              console.log('rollback at create method');
              console.log(aborted);
              rolledBack = true;
            });

            var newquerysql = fs.readFileSync('./modules/users/server/sql/Insert_User.sql').toString();
            var newrequest = new sql.Request(transaction);

            // SQL params
            newrequest.input('userCode', sql.VarChar(50), userCode);
            newrequest.input('firstName', sql.VarChar(50), req.body.firstName);
            newrequest.input('lastName', sql.VarChar(50), req.body.lastName);
            newrequest.input('salesPerson', sql.VarChar(50), req.body.salesPerson.data.CTSTKY);
            newrequest.input('password', sql.VarChar(50), userpassword);
            newrequest.input('email', sql.VarChar(50), req.body.email);

            newrequest.input('passwordReset', sql.VarChar(50), req.body.passwordReset);
            newrequest.input('lastAccessed', sql.DateTime2, new Date());
            newrequest.input('lastRecorded', sql.DateTime2, new Date());
            newrequest.input('lastTransacted', sql.DateTime2, new Date());
            newrequest.input('synced', sql.DateTime2, new Date());

            console.log('==================== insert into IVOS_app_sysuser ======================');
            newrequest.query(newquerysql,
              function(err, newrecordset) {
                if (err) {
                  if (!rolledBack) {
                    transaction.rollback(function(err) {
                      console.log('==================== rolled back ======================');
                      console.log(err);
                    });
                  }
                  console.log(err);
                  console.log('==================== insert into sql database failed ======================');
                  return res.send(err);
                }

                console.log('==================== insert into mongodb ======================');

                var user = new User(req.body);
                user.provider = 'local';
                user.displayName = user.firstName + ' ' + user.lastName;
                user.userCode = userCode;
                user.salesPerson = req.body.salesPerson.data.CTSTKY;
                user.password = req.body.password.toUpperCase();
                user.email = user.email;
                user.firstName = user.firstName;
                user.lastName = user.lastName;

                // Then save the user
                user.save(function (err) {
                  if (err) {
                    console.log('==================== insert into mongodb failed ======================');
                    if (!rolledBack) {
                      transaction.rollback(function(err) {
                        console.log('==================== rolled back ======================');
                        console.log(err);
                      });
                    }

                    return res.status(400).send({
                      message: errorHandler.getErrorMessage(err)
                    });
                  } else {
                    // commit insert
                    transaction.commit(function(err, recordset) {
                      if (err) {
                        if (!rolledBack) {
                          transaction.rollback(function(err) {
                            console.log('==================== rolled back ======================');
                            console.log(err);
                          });
                        }
                      }

                      console.log('Transaction committed.');
                      console.log('==================== successfully insert into sql database ======================');
                    });

                    console.log('==================== successfully insert into mongodb ======================');

                    // Remove sensitive data before login
                    user.password = undefined;
                    user.salt = undefined;

                    console.log('==================== preparing user login ======================');

                    req.login(user, function (err) {
                      if (err) {
                        console.log('==================== user login failed ======================');
                        res.status(400).send(err);
                      } else {
                        console.log('==================== user logged in ======================');
                        res.json({ user: user, warehouse: req.body.warehouse });
                      }
                    });
                  }
                });
              });
          });
        } else {
          console.log('=========== user not found ===============');
        }
      });
  });
};

/**
 * Signin after passport authentication
 */
exports.signin = function (req, res, next) {
  var connection = new sql.Connection(config.mssqldb_application, function(err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    var querysql = fs.readFileSync('./modules/users/server/sql/Get_UserById.sql').toString();

    request.input('userCode', sql.VarChar(50), req.body.username.data.userCode);
    request.query(querysql,
      function(err, recordset) {
        if (err) {
          res.send(err.message);
        } else {
          if (recordset) {
            console.log('=========== user found ===============');

            // user exists, check password
            var userpassword = crypto.createHash('sha1').update('m3J7d9g5W8c0X6vC3wHG6d8n3' + req.body.password.toUpperCase() + '5R4s8CK3h87c3nm9d0PK83vT4', 'utf8').digest('hex');

            if (recordset[0].password !== userpassword) {
              console.log('=========== wrong password ===============');
              res.status(401).send('username and password did not match');
            } else {
              recordset.password = undefined;
              recordset.salt = undefined;

              // check whether user already in mongo if not then add user into mongo
              User.findOne({
                userCode: recordset[0].userCode
              }).exec(function (err, user) {
                if (err) {
                  next(err);
                } else if (user) {
                  // update user login date in IVOS_app_sysuser
                  var transaction = connection.transaction();
                  transaction.begin(function(err) {
                    if (err) {
                      console.log('Begin Error: ' + err);
                    }

                    var inserted = 0;
                    var rolledBack = false;

                    // define transaction rollback function
                    transaction.on('rollback', function(aborted) {
                      console.log('rollback at create method');
                      console.log(aborted);
                      rolledBack = true;
                    });

                    var newRequest = new sql.Request(transaction);
                    var newQuerysql = fs.readFileSync('./modules/users/server/sql/Update_UserLoginDate.sql').toString();

                    // sets SQL params
                    newRequest.input('userCode', sql.VarChar(50), user.userCode);
                    newRequest.input('loginDate', sql.DateTime2, new Date());

                    newRequest.query(newQuerysql,
                    function(err, recordset) {
                      if (err) {
                        if (!rolledBack) {
                          transaction.rollback(function(err) {
                            console.log('==================== rolled back ======================');
                            console.log(err);
                          });
                        }
                        console.log(err);
                        console.log('==================== update sql database failed ======================');
                        return res.send(err);
                      }

                      // if user exists in mongo then login
                      // Remove sensitive data before login
                      user.password = undefined;
                      user.salt = undefined;

                      req.login(user, function (err) {
                        if (err) {
                          res.status(400).send(err);
                        } else {
                          transaction.commit(function(err, recordset) {
                            if (err) {
                              if (!rolledBack) {
                                transaction.rollback(function(err) {
                                  console.log('==================== rolled back ======================');
                                  console.log(err);
                                });
                              }
                            }

                            console.log('Transaction committed. Login Date updated.');
                          });

                          res.json({ user: user, warehouse: req.body.warehouse });
                        }
                      });
                    });
                  });
                } else {
                  // if user is not exists in mongo, then add user first
                  var newuser = new User();
                  newuser.provider = 'local';
                  newuser.displayName = recordset[0].firstName + ' ' + recordset[0].lastName;
                  newuser.userCode = recordset[0].userCode;
                  newuser.salesPerson = recordset[0].salesPerson;
                  newuser.password = req.body.password.toUpperCase();
                  newuser.email = recordset[0].email;
                  newuser.firstName = recordset[0].firstName;
                  newuser.lastName = recordset[0].lastName;

                  console.log('=========== saving newuser to mongo ===============');
                  newuser.save(function (err) {
                    if (err) {
                      console.log(err);
                      res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                      });
                    } else {
                      // update user login date in IVOS_app_sysuser
                      var transaction = connection.transaction();
                      transaction.begin(function(err) {
                        if (err) {
                          console.log('Begin Error: ' + err);
                        }

                        var inserted = 0;
                        var rolledBack = false;

                        // define transaction rollback function
                        transaction.on('rollback', function(aborted) {
                          console.log('rollback at create method');
                          console.log(aborted);
                          rolledBack = true;
                        });

                        var newRequest = new sql.Request(transaction);
                        var newQuerysql = fs.readFileSync('./modules/users/server/sql/Update_UserLoginDate.sql').toString();

                        // sets SQL params
                        newRequest.input('userCode', sql.VarChar(50), newuser.userCode);
                        newRequest.input('loginDate', sql.DateTime2, new Date());

                        newRequest.query(newQuerysql,
                        function(err, recordset) {
                          if (err) {
                            if (!rolledBack) {
                              transaction.rollback(function(err) {
                                console.log('==================== rolled back ======================');
                                console.log(err);
                              });
                            }
                            console.log(err);
                            console.log('==================== update sql database failed ======================');
                            return res.send(err);
                          }

                          // Remove sensitive data before login
                          newuser.password = undefined;
                          newuser.salt = undefined;

                          console.log('=========== newuser data saved ===============');
                          console.log('=========== newuser logging in ===============');

                          req.login(newuser, function (err) {
                            if (err) {
                              res.status(400).send(err);
                            } else {
                              transaction.commit(function(err, recordset) {
                                if (err) {
                                  if (!rolledBack) {
                                    transaction.rollback(function(err) {
                                      console.log('==================== rolled back ======================');
                                      console.log(err);
                                    });
                                  }
                                }

                                console.log('Transaction committed. Login Date updated.');
                              });

                              res.json({ user: newuser, warehouse: req.body.warehouse });
                            }
                          });
                        });
                      });
                      console.log('============= successfully login into the system ===================');
                    }
                  });
                }
              });
            }
          } else {
            console.log('=========== user not found ===============');
          }
        }
      });
  });
};

/**
 * Signout
 */
exports.signout = function (req, res) {
  req.logout();
  res.redirect('/');
};

/**
 * OAuth provider call
 */
exports.oauthCall = function (strategy, scope) {
  return function (req, res, next) {
    // Set redirection path on session.
    // Do not redirect to a signin or signup page
    if (noReturnUrls.indexOf(req.query.redirect_to) === -1) {
      req.session.redirect_to = req.query.redirect_to;
    }
    // Authenticate
    passport.authenticate(strategy, scope)(req, res, next);
  };
};

/**
 * OAuth callback
 */
exports.oauthCallback = function (strategy) {
  return function (req, res, next) {
    // Pop redirect URL from session
    var sessionRedirectURL = req.session.redirect_to;
    delete req.session.redirect_to;

    passport.authenticate(strategy, function (err, user, info) {
      if (err) {
        return res.redirect('/authentication/signin?err=' + encodeURIComponent(errorHandler.getErrorMessage(err)));
      }
      if (!user) {
        return res.redirect('/authentication/signin');
      }
      req.login(user, function (err) {
        if (err) {
          return res.redirect('/authentication/signin');
        }

        return res.redirect(info || sessionRedirectURL || '/');
      });
    })(req, res, next);
  };
};

/**
 * Helper function to save or update a OAuth user profile
 */
exports.saveOAuthUserProfile = function (req, providerUserProfile, done) {
  if (!req.user) {
    // Define a search query fields
    var searchMainProviderIdentifierField = 'providerData.' + providerUserProfile.providerIdentifierField;
    var searchAdditionalProviderIdentifierField = 'additionalProvidersData.' + providerUserProfile.provider + '.' + providerUserProfile.providerIdentifierField;

    // Define main provider search query
    var mainProviderSearchQuery = {};
    mainProviderSearchQuery.provider = providerUserProfile.provider;
    mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

    // Define additional provider search query
    var additionalProviderSearchQuery = {};
    additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

    // Define a search query to find existing user with current provider profile
    var searchQuery = {
      $or: [mainProviderSearchQuery, additionalProviderSearchQuery]
    };

    User.findOne(searchQuery, function (err, user) {
      if (err) {
        return done(err);
      } else {
        if (!user) {
          var possibleUsername = providerUserProfile.userCode || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');

          User.findUniqueUsername(possibleUsername, null, function (availableUsername) {
            user = new User({
              firstName: providerUserProfile.firstName,
              lastName: providerUserProfile.lastName,
              userCode: availableUsername,
              displayName: providerUserProfile.displayName,
              email: providerUserProfile.email,
              profileImageURL: providerUserProfile.profileImageURL,
              provider: providerUserProfile.provider,
              providerData: providerUserProfile.providerData
            });

            // And save the user
            user.save(function (err) {
              return done(err, user);
            });
          });
        } else {
          return done(err, user);
        }
      }
    });
  } else {
    // User is already logged in, join the provider data to the existing user
    var user = req.user;

    // Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
    if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {
      // Add the provider data to the additional provider data field
      if (!user.additionalProvidersData) {
        user.additionalProvidersData = {};
      }

      user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

      // Then tell mongoose that we've updated the additionalProvidersData field
      user.markModified('additionalProvidersData');

      // And save the user
      user.save(function (err) {
        return done(err, user, '/settings/accounts');
      });
    } else {
      return done(new Error('User is already connected using this provider'), user);
    }
  }
};

/**
 * Remove OAuth provider
 */
exports.removeOAuthProvider = function (req, res, next) {
  var user = req.user;
  var provider = req.query.provider;

  if (!user) {
    return res.status(401).json({
      message: 'User is not authenticated'
    });
  } else if (!provider) {
    return res.status(400).send();
  }

  // Delete the additional provider
  if (user.additionalProvidersData[provider]) {
    delete user.additionalProvidersData[provider];

    // Then tell mongoose that we've updated the additionalProvidersData field
    user.markModified('additionalProvidersData');
  }

  user.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      req.login(user, function (err) {
        if (err) {
          return res.status(400).send(err);
        } else {
          return res.json(user);
        }
      });
    }
  });
};
