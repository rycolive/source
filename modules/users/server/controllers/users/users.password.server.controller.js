'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  nodemailer = require('nodemailer'),
  async = require('async'),
  crypto = require('crypto');
var sql = require('mssql');
var fs = require('fs');

var smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function (req, res, next) {
  async.waterfall([
    // Generate random token
    function (done) {
      crypto.randomBytes(20, function (err, buffer) {
        var token = buffer.toString('hex');
        done(err, token);
      });
    },
    // Lookup user by username
    function (token, done) {
      console.log(req.body.username);
      if (req.body.username) {
        // get user by username from IVOS_app_sysuser
        // connect to db RHAU_Application
        var connection = new sql.Connection(config.mssqldb_application, function(err) {
          if (err) {
            return res.send(err.message);
          }

          // get user by id query
          var request = new sql.Request(connection);
          var querysql = fs.readFileSync('./modules/users/server/sql/Get_UserById.sql').toString();

          // sets SQL params
          request.input('userCode', sql.VarChar(50), req.body.username);

          // execute query
          request.query(querysql,
            function(err, recordset) {
              if (err) {
                console.log('==================== get user by userCode failed ======================');
                return res.send(err.message);
              } else if (recordset.length !== 0) {
                console.log('==================== get user by userCode ======================');
                User.findOne({
                  userCode: recordset[0].userCode
                }).exec(function (err, user) {
                  if (err) {
                    next(err);
                  } else if (user) {
                    console.log('user exists in mongodb');
                    // saving user password token
                    user.resetPasswordToken = token;

                    // sets token expired in 1 hour
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    // save user
                    user.save(function (err) {
                      if (err) {
                        console.log(err);
                        res.status(400).send({
                          message: errorHandler.getErrorMessage(err)
                        });
                      } else {
                        // Remove sensitive data before continue
                        user.password = undefined;
                        user.salt = undefined;

                        done(err, token, recordset, user);
                      }
                    });
                  } else {
                    console.log('user not exists in mongodb');
                    // if user is not exists in mongo, then add user first
                    var newuser = new User();
                    newuser.provider = 'local';
                    newuser.displayName = recordset[0].firstName + ' ' + recordset[0].lastName;
                    newuser.userCode = recordset[0].userCode;
                    newuser.salesPerson = recordset[0].salesPerson;
                    newuser.password = req.body.password.toUpperCase();
                    newuser.email = recordset[0].email;
                    newuser.firstName = recordset[0].firstName;
                    newuser.lastName = recordset[0].lastName;
                    newuser.resetPasswordToken = token;
                    newuser.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    console.log('=========== saving newuser to mongo ===============');
                    newuser.save(function (err) {
                      if (err) {
                        console.log(err);
                        res.status(400).send({
                          message: errorHandler.getErrorMessage(err)
                        });
                      } else {
                        // Remove sensitive data before continue
                        newuser.password = undefined;
                        newuser.salt = undefined;

                        done(err, token, recordset, newuser);
                      }
                    });
                  }
                });
              } else {
                return res.status(400).send({
                  message: 'No account with that username has been found'
                });
              }
            });
        });
      } else {
        return res.status(400).send({
          message: 'Username field must not be blank'
        });
      }
    },
    function (token, recordset, newuser, done) {
      var httpTransport = 'http://';
      if (config.secure && config.secure.ssl === true) {
        httpTransport = 'https://';
      }

      res.render(path.resolve('modules/users/server/templates/reset-password-email'), {
        name: recordset[0].firstName + ' ' + recordset[0].lastName,
        appName: config.app.title,
        url: httpTransport + req.headers.host + '/api/auth/reset/' + token
      }, function (err, emailHTML) {
        done(err, emailHTML, recordset, newuser);
      });
    },
    // If valid email, send reset email using service
    function (emailHTML, recordset, newuser, done) {
      var mailOptions = {
        to: recordset[0].email,
        from: config.mailer.from,
        subject: 'RYCO LIVE 247 Password Reset',
        html: emailHTML
      };

      smtpTransport.sendMail(mailOptions, function (err) {
        if (!err) {
          res.send({
            message: 'An email has been sent to the provided email with further instructions.',
            user: newuser
          });
        } else {
          console.log(err);
          return res.status(400).send({
            message: 'Failure sending email'
          });
        }

        done(err);
      });
    }
  ], function (err) {
    if (err) {
      return next(err);
    }
  });
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function (req, res) {
  User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: {
      $gt: Date.now()
    }
  }, function (err, user) {
    if (err || !user) {
      return res.redirect('/password/reset/invalid');
    }

    res.redirect('/password/reset/' + req.params.token);
  });
};

/**
 * Reset password POST from email token
 */
exports.reset = function (req, res, next) {
  // Init Variables
  var passwordDetails = req.body;

  async.waterfall([

    function (done) {
      User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {
          $gt: Date.now()
        }
      }, function (err, user) {
        if (!err && user) {
          if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
            // save new user into IVOS_app_sysuser and mongodb
            user.password = passwordDetails.newPassword.toUpperCase();
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            var connection = new sql.Connection(config.mssqldb_application, function(err) {
              if (err) {
                return res.send(err.message);
              }

              var transaction = connection.transaction();
              transaction.begin(function(err) {
                if (err) {
                  console.log('Begin Error: ' + err);
                }

                var inserted = 0;
                var rolledBack = false;

                // define transaction rollback function
                transaction.on('rollback', function(aborted) {
                  console.log('rollback at create method');
                  console.log(aborted);
                  rolledBack = true;
                });

                // get user by id query
                var request = new sql.Request(transaction);
                var querysql = fs.readFileSync('./modules/users/server/sql/Update_UserPassword.sql').toString();
                var userPassword = crypto.createHash('sha1').update('m3J7d9g5W8c0X6vC3wHG6d8n3' + passwordDetails.newPassword.toUpperCase() + '5R4s8CK3h87c3nm9d0PK83vT4', 'utf8').digest('hex');

                // sets SQL params
                request.input('userCode', sql.VarChar(50), user.userCode);
                request.input('newpassword', sql.VarChar(50), userPassword);

                // saving to IVOS_app_sysuser
                request.query(querysql,
                  function(err, recordset) {
                    if (err) {
                      if (!rolledBack) {
                        transaction.rollback(function(err) {
                          console.log('==================== rolled back ======================');
                          console.log(err);
                        });
                      }
                      console.log(err);
                      console.log('==================== update sql database failed ======================');
                      return res.send(err);
                    }

                    // saving to mongoDB
                    user.save(function (err) {
                      if (err) {
                        if (!rolledBack) {
                          transaction.rollback(function(err) {
                            console.log('==================== rolled back ======================');
                            console.log(err);
                          });
                        }

                        return res.status(400).send({
                          message: errorHandler.getErrorMessage(err)
                        });
                      } else {
                        // commit update password
                        transaction.commit(function(err, recordset) {
                          if (err) {
                            if (!rolledBack) {
                              transaction.rollback(function(err) {
                                console.log('==================== rolled back ======================');
                                console.log(err);
                              });
                            }
                          }

                          console.log('Transaction committed. Password updated.');
                        });

                        req.login(user, function (err) {
                          if (err) {
                            res.status(400).send(err);
                          } else {
                            // Remove sensitive data before return authenticated user
                            user.password = undefined;
                            user.salt = undefined;

                            res.json(user);

                            done(err, user);
                          }
                        });
                      }
                    });
                  });
              });
            });
          } else {
            return res.status(400).send({
              message: 'Passwords do not match'
            });
          }
        } else {
          return res.status(400).send({
            message: 'Password reset token is invalid or has expired.'
          });
        }
      });
    },
    function (user, done) {
      res.render('modules/users/server/templates/reset-password-confirm-email', {
        name: user.displayName,
        appName: config.app.title
      }, function (err, emailHTML) {
        done(err, emailHTML, user);
      });
    },
    // If valid email, send reset email using service
    function (emailHTML, user, done) {
      var mailOptions = {
        to: user.email,
        from: config.mailer.from,
        subject: 'Your password has been changed',
        html: emailHTML
      };

      smtpTransport.sendMail(mailOptions, function (err) {
        done(err, 'done');
      });
    }
  ], function (err) {
    if (err) {
      return next(err);
    }
  });
};

/**
 * Change Password
 */
exports.changePassword = function (req, res, next) {
  // Init Variables
  var passwordDetails = req.body;

  if (req.user) {
    if (passwordDetails.newPassword) {
      // find user in mongodb
      User.findOne({ userCode: req.user.userCode }, function (err, user) {
        if (!err && user) {
          // check if current password is correct
          var saltPassword = 'm3J7d9g5W8c0X6vC3wHG6d8n3' + passwordDetails.currentPassword.toUpperCase() + '5R4s8CK3h87c3nm9d0PK83vT4';
          if (user.authenticate(saltPassword)) {
            // check new password === verify password
            if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
              user.password = passwordDetails.newPassword.toUpperCase();

              // update user in IVOS_app_sysuser
              var connection = new sql.Connection(config.mssqldb_application, function(err) {
                if (err) {
                  return res.send(err.message);
                }

                var transaction = connection.transaction();
                transaction.begin(function(err) {
                  if (err) {
                    console.log('Begin Error: ' + err);
                  }

                  var inserted = 0;
                  var rolledBack = false;

                  // define transaction rollback function
                  transaction.on('rollback', function(aborted) {
                    console.log('rollback at create method');
                    console.log(aborted);
                    rolledBack = true;
                  });

                  // get user by id query
                  var request = new sql.Request(transaction);
                  var querysql = fs.readFileSync('./modules/users/server/sql/Update_UserPassword.sql').toString();
                  var userPassword = crypto.createHash('sha1').update('m3J7d9g5W8c0X6vC3wHG6d8n3' + passwordDetails.newPassword.toUpperCase() + '5R4s8CK3h87c3nm9d0PK83vT4', 'utf8').digest('hex');

                  // sets SQL params
                  request.input('userCode', sql.VarChar(50), user.userCode);
                  request.input('newpassword', sql.VarChar(50), userPassword);

                  // saving to IVOS_app_sysuser
                  request.query(querysql,
                    function(err, recordset) {
                      if (err) {
                        if (!rolledBack) {
                          transaction.rollback(function(err) {
                            console.log('==================== rolled back ======================');
                            console.log(err);
                          });
                        }
                        console.log(err);
                        console.log('==================== update sql database failed ======================');
                        return res.send(err);
                      }

                      // update user in mongodb
                      user.save(function (err) {
                        if (err) {
                          return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                          });
                        } else {
                          transaction.commit(function(err, recordset) {
                            if (err) {
                              if (!rolledBack) {
                                transaction.rollback(function(err) {
                                  console.log('==================== rolled back ======================');
                                  console.log(err);
                                });
                              }
                            }

                            console.log('Transaction committed. Password updated.');
                          });

                          req.login(user, function (err) {
                            if (err) {
                              res.status(400).send(err);
                            } else {
                              res.send({
                                message: 'Password changed successfully'
                              });
                            }
                          });
                        }
                      });
                    });
                });
              });
            } else {
              res.status(400).send({
                message: 'Passwords do not match'
              });
            }
          } else {
            res.status(400).send({
              message: 'Current password is incorrect'
            });
          }
        } else {
          res.status(400).send({
            message: 'User is not found'
          });
        }
      });
    } else {
      res.status(400).send({
        message: 'Please provide a new password'
      });
    }
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};
