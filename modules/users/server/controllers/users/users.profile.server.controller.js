'use strict';

/**
 * Module dependencies
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  multer = require('multer'),
  config = require(path.resolve('./config/config')),
  User = mongoose.model('User');
var sql = require('mssql');

/**
 * Update user details
 */
exports.update = function (req, res) {
  // Init Variables
  var user = req.user;

  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;

  // For security measurement do not use _id from the req.body object
  delete req.body._id;

  if (user) {
    // Merge existing user
    user = _.extend(user, req.body);
    user.updated = Date.now();
    user.displayName = user.firstName + ' ' + user.lastName;

    var connection = new sql.Connection(config.mssqldb_application, function(err) {
      if (err) {
        return res.send(err.message);
      }

      var transaction = connection.transaction();
      transaction.begin(function(err) {
        if (err) {
          console.log('Begin Error: ' + err);
        }

        var inserted = 0;
        var rolledBack = false;

        // define transaction rollback function
        transaction.on('rollback', function(aborted) {
          console.log('rollback at create method');
          console.log(aborted);
          rolledBack = true;
        });

        // get user by id query
        var request = new sql.Request(transaction);
        var querysql = fs.readFileSync('./modules/users/server/sql/Update_UserProfile.sql').toString();

        // sets SQL params
        request.input('userCode', sql.VarChar(50), user.userCode);
        request.input('firstName', sql.VarChar(50), user.firstName);
        request.input('lastName', sql.VarChar(50), user.lastName);
        request.input('email', sql.VarChar(50), user.email);

        request.query(querysql,
          function(err, recordset) {
            if (err) {
              if (!rolledBack) {
                transaction.rollback(function(err) {
                  console.log('==================== rolled back ======================');
                  console.log(err);
                });
              }
              console.log(err);
              console.log('==================== update sql database failed ======================');
              return res.send(err);
            }

            user.save(function (err) {
              if (err) {
                return res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              } else {
                // commit update password
                transaction.commit(function(err, recordset) {
                  if (err) {
                    if (!rolledBack) {
                      transaction.rollback(function(err) {
                        console.log('==================== rolled back ======================');
                        console.log(err);
                      });
                    }
                  }

                  console.log('Transaction committed. Password updated.');

                  req.login(user, function (err) {
                    if (err) {
                      res.status(400).send(err);
                    } else {
                      res.json(user);
                    }
                  });
                });
              }
            });
          });
      });
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};

/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
  var user = req.user;
  var upload = multer(config.uploads.profileUpload).single('newProfilePicture');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;

  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (user) {
    upload(req, res, function (uploadError) {
      if (uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading profile picture'
        });
      } else {
        user.profileImageURL = config.uploads.profileUpload.dest + req.file.filename;

        user.save(function (saveError) {
          if (saveError) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(saveError)
            });
          } else {
            req.login(user, function (err) {
              if (err) {
                res.status(400).send(err);
              } else {
                res.json(user);
              }
            });
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};

/**
 * Send User
 */
exports.me = function (req, res) {
  res.json(req.user || null);
};
