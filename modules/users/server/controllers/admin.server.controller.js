'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
var sql = require('mssql');
var config = require(path.resolve('./config/config'));
var async = require('async');
var fs = require('fs');

/**
 * Show the current user
 */
exports.read = function (req, res) {
  res.json(req.model);
};

/**
 * Update a User
 */
exports.update = function (req, res) {
  var user = req.model;

  // For security purposes only merge these parameters
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.displayName = user.firstName + ' ' + user.lastName;
  user.roles = req.body.roles;

  user.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    res.json(user);
  });
};

/**
 * Delete a user
 */
exports.delete = function (req, res) {
  var user = req.model;

  user.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    res.json(user);
  });
};

/**
 * List of Users
 */
exports.list = function (req, res) {
  /* async.waterfall([
    function (done) {
      sql.setDefaultConfig(config.mssqldb_application);
      done();
    },
    function (done) {
      sql.execute({
        query: sql.fromFile('../sql/GetAll_User')
      }).then(function (result) {
        res.json(result);
      }, function (error) {
        console.log(error);
        return res.send(error.message);
      });
      done();
    }], function(err) {
    if (err) {
      console.log('failed update: ' + err);
      return;
    }
  });*/
  var querysql = fs.readFileSync('./modules/users/server/sql/GetAll_User.sql').toString();
  var connection = new sql.Connection(config.mssqldb_application, function (err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};

/**
 * User middleware
 */
exports.userByID = function (req, res, next, id) {
  console.log('here');
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'User is invalid'
    });
  }

  User.findById(id, '-salt -password').exec(function (err, user) {
    if (err) {
      return next(err);
    } else if (!user) {
      return next(new Error('Failed to load user ' + id));
    }

    req.model = user;
    next();
  });
};

exports.userBySalesPerson = function (req, res) {
  var querysql = fs.readFileSync('./modules/users/server/sql/Get_UserBySalesPerson.sql').toString();
  var connection = new sql.Connection(config.mssqldb_application, function(err) {
    if (err) {
      return res.send(err.message);
    }

    var request = new sql.Request(connection);
    request.input('salesPersonId', sql.VarChar(50), req.params.salesPersonId);

    request.query(querysql,
      function(err, recordset) {
        if (err) {
          return res.send(err.message);
        } else {
          res.json(recordset);
        }
      });
  });
};
