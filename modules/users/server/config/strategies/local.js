'use strict';

/**
 * Module dependencies
 */
var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  User = require('mongoose').model('User');

module.exports = function () {
  // Use local strategy
  passport.use(new LocalStrategy({
    usernameField: 'userCode',
    passwordField: 'password'
  },
  function (userCode, password, done) {
    User.findOne({
      userCode: userCode.toUpperCase()
    }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user || !user.authenticate(password)) {
        return done(null, false, {
          message: 'Invalid userCode or password'
        });
      }

      return done(null, user);
    });
  }));
};
