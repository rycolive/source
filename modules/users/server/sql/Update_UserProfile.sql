UPDATE
	IVOS_app_sysuser
SET
	firstName = @firstName,
	lastName = @lastName,
	email = @email
WHERE
	userCode = @userCode