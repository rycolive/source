SELECT
	TOP 1 userCode
FROM
	IVOS_app_sysuser
WHERE
	salesPerson = @salesPerson
ORDER BY
	userCode DESC