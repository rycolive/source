SELECT
	salesPerson,
	userCode,
	firstName,
	lastName,
	email,
	password
FROM
	IVOS_app_sysuser
WHERE
	userCode = @userCode