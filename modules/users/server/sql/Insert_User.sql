INSERT INTO [dbo].[IVOS_app_sysuser]
           ([SalesPerson]
           ,[UserCode]
           ,[FirstName]
           ,[LastName]
           ,[Password]
           ,[Email]
           ,[PasswordResetRequired]
           ,[LastAccessed]
           ,[Recorded]
           ,[Transacted]
           ,[Synced])
     VALUES
           (@salesPerson
           , @userCode
           , @firstName
           , @lastName
           , @password
           , @email
           , @passwordReset
           , @lastAccessed
           , @lastRecorded
           , @lastTransacted
           , @synced)