// Productreplenishments service used to communicate Productreplenishments REST endpoints
(function () {
  'use strict';

  angular
    .module('productreplenishments')
    .factory('ProductreplenishmentsService', ProductreplenishmentsService);

  ProductreplenishmentsService.$inject = ['$resource'];

  function ProductreplenishmentsService($resource) {
    return $resource('api/productreplenishments/:productreplenishmentId', {
      productreplenishmentId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
