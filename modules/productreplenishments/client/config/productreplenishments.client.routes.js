(function () {
  'use strict';

  angular
    .module('productreplenishments')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('productreplenishments', {
        abstract: true,
        url: '/productreplenishments',
        template: '<ui-view/>'
      })
      .state('productreplenishments.list', {
        url: '',
        templateUrl: 'modules/productreplenishments/client/views/list-productreplenishments.client.view.html',
        controller: 'ProductreplenishmentsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Productreplenishments List'
        },
        breadcrumb: {
          label: 'Stock Request History',
          parent: 'home'
        }
      })
      .state('productreplenishments.create', {
        url: '/create',
        templateUrl: 'modules/productreplenishments/client/views/form-productreplenishment.client.view.html',
        controller: 'ProductreplenishmentsController',
        controllerAs: 'vm',
        resolve: {
          productreplenishmentResolve: newProductreplenishment
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Productreplenishments Create'
        },
        breadcrumb: {
          label: 'Create Stock Replenishment',
          parent: 'home'
        }
      })
      .state('productreplenishments.edit', {
        url: '/:productreplenishmentId/edit',
        templateUrl: 'modules/productreplenishments/client/views/form-productreplenishment.client.view.html',
        controller: 'ProductreplenishmentsController',
        controllerAs: 'vm',
        resolve: {
          productreplenishmentResolve: getProductreplenishment
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Productreplenishment {{ productreplenishmentResolve.name }}'
        }
      })
      .state('productreplenishments.view', {
        url: '/:productreplenishmentId',
        templateUrl: 'modules/productreplenishments/client/views/view-productreplenishment.client.view.html',
        controller: 'ProductreplenishmentsController',
        controllerAs: 'vm',
        resolve: {
          productreplenishmentResolve: getProductreplenishment
        },
        data: {
          pageTitle: 'Productreplenishment {{ articleResolve.name }}'
        }
      });
  }

  getProductreplenishment.$inject = ['$stateParams', 'ProductreplenishmentsService'];

  function getProductreplenishment($stateParams, ProductreplenishmentsService) {
    return ProductreplenishmentsService.get({
      productreplenishmentId: $stateParams.productreplenishmentId
    }).$promise;
  }

  newProductreplenishment.$inject = ['ProductreplenishmentsService'];

  function newProductreplenishment(ProductreplenishmentsService) {
    return new ProductreplenishmentsService();
  }
}());
