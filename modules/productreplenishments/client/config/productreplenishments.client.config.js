(function () {
  'use strict';

  angular
    .module('productreplenishments')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Stock Replenishment',
      state: 'productreplenishments',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'productreplenishments', {
      title: 'Stock Request History',
      state: 'productreplenishments.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'productreplenishments', {
      title: 'Create Stock Request',
      state: 'productreplenishments.create',
      roles: ['user']
    });
  }
}());
