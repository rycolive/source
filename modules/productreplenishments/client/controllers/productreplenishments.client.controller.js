(function () {
  'use strict';

  // Productreplenishments controller
  angular
    .module('productreplenishments')
    .controller('ProductreplenishmentsController', ProductreplenishmentsController);

  ProductreplenishmentsController.$inject = ['$scope', '$state', 'Authentication', 'productreplenishmentResolve'];

  function ProductreplenishmentsController ($scope, $state, Authentication, productreplenishment) {
    var vm = this;

    vm.authentication = Authentication;
    vm.productreplenishment = productreplenishment;
    vm.error = null;
    vm.save = save;

    // Save Productreplenishment
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.productreplenishmentForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.productreplenishment._id) {
        vm.productreplenishment.$update(successCallback, errorCallback);
      } else {
        vm.productreplenishment.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('productreplenishments.view', {
          productreplenishmentId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
