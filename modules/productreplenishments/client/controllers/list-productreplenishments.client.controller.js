(function () {
  'use strict';

  angular
    .module('productreplenishments')
    .controller('ProductreplenishmentsListController', ProductreplenishmentsListController);

  ProductreplenishmentsListController.$inject = ['ProductreplenishmentsService'];

  function ProductreplenishmentsListController(ProductreplenishmentsService) {
    var vm = this;

    vm.productreplenishments = ProductreplenishmentsService.query();
  }
}());
