'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Productreplenishment Schema
 */
var ProductreplenishmentSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Productreplenishment name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Productreplenishment', ProductreplenishmentSchema);
