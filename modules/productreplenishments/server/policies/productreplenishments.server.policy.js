'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Productreplenishments Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/productreplenishments',
      permissions: '*'
    }, {
      resources: '/api/productreplenishments/:productreplenishmentId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/productreplenishments',
      permissions: ['get', 'post']
    }, {
      resources: '/api/productreplenishments/:productreplenishmentId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Productreplenishments Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? ['user'] : ['guest'];

  // If an Productreplenishment is being processed and the current user created it then allow any manipulation
  if (req.productreplenishment && req.user && req.productreplenishment.user && req.productreplenishment.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
