'use strict';

/**
 * Module dependencies
 */
var productreplenishmentsPolicy = require('../policies/productreplenishments.server.policy'),
  productreplenishments = require('../controllers/productreplenishments.server.controller');

module.exports = function(app) {
  // Productreplenishments Routes
  app.route('/api/productreplenishments').all(productreplenishmentsPolicy.isAllowed)
    .get(productreplenishments.list)
    .post(productreplenishments.create);

  app.route('/api/productreplenishments/:productreplenishmentId').all(productreplenishmentsPolicy.isAllowed)
    .get(productreplenishments.read)
    .put(productreplenishments.update)
    .delete(productreplenishments.delete);

  // Finish by binding the Productreplenishment middleware
  app.param('productreplenishmentId', productreplenishments.productreplenishmentByID);
};
