'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Productreplenishment = mongoose.model('Productreplenishment'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Productreplenishment
 */
exports.create = function(req, res) {
  var productreplenishment = new Productreplenishment(req.body);
  productreplenishment.user = req.user;

  productreplenishment.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(productreplenishment);
    }
  });
};

/**
 * Show the current Productreplenishment
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var productreplenishment = req.productreplenishment ? req.productreplenishment.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  productreplenishment.isCurrentUserOwner = req.user && productreplenishment.user && productreplenishment.user._id.toString() === req.user._id.toString();

  res.jsonp(productreplenishment);
};

/**
 * Update a Productreplenishment
 */
exports.update = function(req, res) {
  var productreplenishment = req.productreplenishment;

  productreplenishment = _.extend(productreplenishment, req.body);

  productreplenishment.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(productreplenishment);
    }
  });
};

/**
 * Delete an Productreplenishment
 */
exports.delete = function(req, res) {
  var productreplenishment = req.productreplenishment;

  productreplenishment.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(productreplenishment);
    }
  });
};

/**
 * List of Productreplenishments
 */
exports.list = function(req, res) {
  Productreplenishment.find().sort('-created').populate('user', 'displayName').exec(function(err, productreplenishments) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(productreplenishments);
    }
  });
};

/**
 * Productreplenishment middleware
 */
exports.productreplenishmentByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Productreplenishment is invalid'
    });
  }

  Productreplenishment.findById(id).populate('user', 'displayName').exec(function (err, productreplenishment) {
    if (err) {
      return next(err);
    } else if (!productreplenishment) {
      return res.status(404).send({
        message: 'No Productreplenishment with that identifier has been found'
      });
    }
    req.productreplenishment = productreplenishment;
    next();
  });
};
