'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Productreplenishment = mongoose.model('Productreplenishment'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  productreplenishment;

/**
 * Productreplenishment routes tests
 */
describe('Productreplenishment CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Productreplenishment
    user.save(function () {
      productreplenishment = {
        name: 'Productreplenishment name'
      };

      done();
    });
  });

  it('should be able to save a Productreplenishment if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Productreplenishment
        agent.post('/api/productreplenishments')
          .send(productreplenishment)
          .expect(200)
          .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
            // Handle Productreplenishment save error
            if (productreplenishmentSaveErr) {
              return done(productreplenishmentSaveErr);
            }

            // Get a list of Productreplenishments
            agent.get('/api/productreplenishments')
              .end(function (productreplenishmentsGetErr, productreplenishmentsGetRes) {
                // Handle Productreplenishment save error
                if (productreplenishmentsGetErr) {
                  return done(productreplenishmentsGetErr);
                }

                // Get Productreplenishments list
                var productreplenishments = productreplenishmentsGetRes.body;

                // Set assertions
                (productreplenishments[0].user._id).should.equal(userId);
                (productreplenishments[0].name).should.match('Productreplenishment name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Productreplenishment if not logged in', function (done) {
    agent.post('/api/productreplenishments')
      .send(productreplenishment)
      .expect(403)
      .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
        // Call the assertion callback
        done(productreplenishmentSaveErr);
      });
  });

  it('should not be able to save an Productreplenishment if no name is provided', function (done) {
    // Invalidate name field
    productreplenishment.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Productreplenishment
        agent.post('/api/productreplenishments')
          .send(productreplenishment)
          .expect(400)
          .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
            // Set message assertion
            (productreplenishmentSaveRes.body.message).should.match('Please fill Productreplenishment name');

            // Handle Productreplenishment save error
            done(productreplenishmentSaveErr);
          });
      });
  });

  it('should be able to update an Productreplenishment if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Productreplenishment
        agent.post('/api/productreplenishments')
          .send(productreplenishment)
          .expect(200)
          .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
            // Handle Productreplenishment save error
            if (productreplenishmentSaveErr) {
              return done(productreplenishmentSaveErr);
            }

            // Update Productreplenishment name
            productreplenishment.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Productreplenishment
            agent.put('/api/productreplenishments/' + productreplenishmentSaveRes.body._id)
              .send(productreplenishment)
              .expect(200)
              .end(function (productreplenishmentUpdateErr, productreplenishmentUpdateRes) {
                // Handle Productreplenishment update error
                if (productreplenishmentUpdateErr) {
                  return done(productreplenishmentUpdateErr);
                }

                // Set assertions
                (productreplenishmentUpdateRes.body._id).should.equal(productreplenishmentSaveRes.body._id);
                (productreplenishmentUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Productreplenishments if not signed in', function (done) {
    // Create new Productreplenishment model instance
    var productreplenishmentObj = new Productreplenishment(productreplenishment);

    // Save the productreplenishment
    productreplenishmentObj.save(function () {
      // Request Productreplenishments
      request(app).get('/api/productreplenishments')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Productreplenishment if not signed in', function (done) {
    // Create new Productreplenishment model instance
    var productreplenishmentObj = new Productreplenishment(productreplenishment);

    // Save the Productreplenishment
    productreplenishmentObj.save(function () {
      request(app).get('/api/productreplenishments/' + productreplenishmentObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', productreplenishment.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Productreplenishment with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/productreplenishments/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Productreplenishment is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Productreplenishment which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Productreplenishment
    request(app).get('/api/productreplenishments/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Productreplenishment with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Productreplenishment if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Productreplenishment
        agent.post('/api/productreplenishments')
          .send(productreplenishment)
          .expect(200)
          .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
            // Handle Productreplenishment save error
            if (productreplenishmentSaveErr) {
              return done(productreplenishmentSaveErr);
            }

            // Delete an existing Productreplenishment
            agent.delete('/api/productreplenishments/' + productreplenishmentSaveRes.body._id)
              .send(productreplenishment)
              .expect(200)
              .end(function (productreplenishmentDeleteErr, productreplenishmentDeleteRes) {
                // Handle productreplenishment error error
                if (productreplenishmentDeleteErr) {
                  return done(productreplenishmentDeleteErr);
                }

                // Set assertions
                (productreplenishmentDeleteRes.body._id).should.equal(productreplenishmentSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Productreplenishment if not signed in', function (done) {
    // Set Productreplenishment user
    productreplenishment.user = user;

    // Create new Productreplenishment model instance
    var productreplenishmentObj = new Productreplenishment(productreplenishment);

    // Save the Productreplenishment
    productreplenishmentObj.save(function () {
      // Try deleting Productreplenishment
      request(app).delete('/api/productreplenishments/' + productreplenishmentObj._id)
        .expect(403)
        .end(function (productreplenishmentDeleteErr, productreplenishmentDeleteRes) {
          // Set message assertion
          (productreplenishmentDeleteRes.body.message).should.match('User is not authorized');

          // Handle Productreplenishment error error
          done(productreplenishmentDeleteErr);
        });

    });
  });

  it('should be able to get a single Productreplenishment that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Productreplenishment
          agent.post('/api/productreplenishments')
            .send(productreplenishment)
            .expect(200)
            .end(function (productreplenishmentSaveErr, productreplenishmentSaveRes) {
              // Handle Productreplenishment save error
              if (productreplenishmentSaveErr) {
                return done(productreplenishmentSaveErr);
              }

              // Set assertions on new Productreplenishment
              (productreplenishmentSaveRes.body.name).should.equal(productreplenishment.name);
              should.exist(productreplenishmentSaveRes.body.user);
              should.equal(productreplenishmentSaveRes.body.user._id, orphanId);

              // force the Productreplenishment to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Productreplenishment
                    agent.get('/api/productreplenishments/' + productreplenishmentSaveRes.body._id)
                      .expect(200)
                      .end(function (productreplenishmentInfoErr, productreplenishmentInfoRes) {
                        // Handle Productreplenishment error
                        if (productreplenishmentInfoErr) {
                          return done(productreplenishmentInfoErr);
                        }

                        // Set assertions
                        (productreplenishmentInfoRes.body._id).should.equal(productreplenishmentSaveRes.body._id);
                        (productreplenishmentInfoRes.body.name).should.equal(productreplenishment.name);
                        should.equal(productreplenishmentInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Productreplenishment.remove().exec(done);
    });
  });
});
