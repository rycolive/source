'use strict';

var defaultEnvConfig = require('./default');

module.exports = {
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/rycolive-mongodev',
    options: {
      user: '',
      pass: ''
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  mssqldb_application: {
    user: 'sa',
    password: 'mitrais',
    server: 'MTPC601',
    database: 'RHAU_Application',
    pool: {
      max: 10
    }
  },
  mssqldb_m3: {
    user: 'sa',
    password: 'mitrais',
    server: 'MTPC601',
    database: 'RHAU_M3_Deve',
    pool: {
      max: 10
    }
  },
  mssqldb_cat: {
    user: 'sa',
    password: 'mitrais',
    server: 'MTPC601',
    database: 'RHAU_Live_Catalog',
    pool: {
      max: 10
    }
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    fileLogger: {
      directoryPath: process.cwd(),
      fileName: 'app.log',
      maxsize: 10485760,
      maxFiles: 2,
      json: false
    }
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Development Environment'
  },
  mailer: {
    service: 'Outlook365',
    from: 'rhgpnoreply@ryco.com.au',
    options: {
      port: 587,
      host: 'smtp.office365.com',
      debug: true,
      logger: true,
      auth: {
        user: 'rhgpservice@ryco.com.au',
        pass: 'Ryco@dmin1247!'
      }
    }
  },
  livereload: true,
  seedDB: {
    seed: process.env.MONGO_SEED === 'true',
    options: {
      logResults: process.env.MONGO_SEED_LOG_RESULTS !== 'false',
      seedUser: {
        username: process.env.MONGO_SEED_USER_USERNAME || 'user',
        provider: 'local',
        email: process.env.MONGO_SEED_USER_EMAIL || 'user@localhost.com',
        firstName: 'User',
        lastName: 'Local',
        displayName: 'User Local',
        roles: ['user']
      },
      seedAdmin: {
        username: process.env.MONGO_SEED_ADMIN_USERNAME || 'admin',
        provider: 'local',
        email: process.env.MONGO_SEED_ADMIN_EMAIL || 'admin@localhost.com',
        firstName: 'Admin',
        lastName: 'Local',
        displayName: 'Admin Local',
        roles: ['user', 'admin']
      }
    }
  }
};
